var config = require('./config'),
    express = require('express'),
    bodyParser = require('body-parser');

module.exports = function() {
    var app = express();
    var passport = require('passport'),
        flash = require('connect-flash')
        session = require('express-session');
    //npm install body-parser
    //op deze manier werkt het curl commando of postman's POST
    app.use(bodyParser.urlencoded({
        extended: true
    }));
    //verhoog de limiet van de dingen die je kan opslaan in de db (de request die je kan sturen)
    app.use(bodyParser.json({limit: '50mb'}));
    app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
    app.use(bodyParser.json());
    
    app.use(passport.initialize());
    app.use(passport.session());
    
    app.use(flash());
    
    app.use(session({
        saveUninitialized: true,
        resave: true,
        secret: 'OurSuperSecretCookieSecret'
    }));
    
    app.set('views', './app/views');
    app.set('view engine', 'ejs'); 
    
    require('../app/routes/index.server.routes.js')(app);
    require('../app/routes/users.server.routes.js')(app);
    require('../app/routes/todos.server.routes.js')(app);
    require('../app/routes/notes.server.routes.js')(app);
    require('../app/routes/gamification.server.routes.js')(app);
    require('../app/routes/studies.server.routes.js')(app);
    require('../app/routes/recordings.server.routes.js')(app);
    require('../app/routes/chatUsers.server.routes.js')(app);
    require('../app/routes/testmoments.server.routes.js')(app);
    
    app.use(express.static('./public'));
    
    return app;
}