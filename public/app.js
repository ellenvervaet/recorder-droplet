;(function () {
    'use strict';
    
    var app = angular.module('app', [
        'ngResource', 
        'ngRoute', 
        'ngDialog',
        'app.users',
        'pubnub.angular.service',
        'app.notes',
        'app.studies',
        'app.recordings',
        'app.tests',
        'app.feedback',
        'app.backoffice'
        ]);
        
    angular.module('app.users', []);
    angular.module('app.notes', []);
    angular.module('app.studies', []);
    angular.module('app.recordings', []);
    angular.module('app.feedback', []);
    angular.module('app.tests', []);
    angular.module('app.backoffice', []);
    

    app.config(['$locationProvider', function($locationProvider) {
        $locationProvider.hashPrefix('!');
        }
    ]);
    
    var app_backoffice = angular.module('app_backoffice', [
        'ngResource', 
        'ngRoute', 
        'app_backoffice.home',
        'app_backoffice.users',
        'app_backoffice.studies',
        'app_backoffice.recordings',
    ]);
    
    angular.module('app_backoffice.home', []);
    angular.module('app_backoffice.users', []);
    angular.module('app_backoffice.studies', []);
    angular.module('app_backoffice.recordings', []);
    
    app_backoffice.config(['$locationProvider', function($locationProvider) {
        $locationProvider.hashPrefix('!');
        }
    ]);
    
    if (window.location.hash === '#_=_')
        window.location.hash = '#!';

    angular.element(document).ready(function(){
        angular.bootstrap(document, 'app');
    });

})();

;(function() {
    'use strict';
    
    angular.module('app.studies')
    
    .controller('MenuCtrl', MenuCtrl);
    
    //PickAStudyController.$inject['$scope', '$routeParams', '$location', 'Authentication'];
     
    function MenuCtrl($scope, NoteService, $rootScope) {
        NoteService.CountLearnedNotes(true, $scope.user).then(
            function(response){$scope.altNotes = response
        });
        NoteService.CountLearnedNotes(false, $scope.user).then(
            function(response){$scope.descentNotes = response
        });
        
        //percentage berekenen punten level
        var fullwidth = $scope.user.level.points;
        var earnedWidth = $scope.user.profile.points; 
        $scope.earnedPercent = earnedWidth/fullwidth*100;
        
        $( document ).ready(function() {
            $('#percent').width($scope.earnedPercent + '%');
        });
    }
    
})();

angular.module('todos', []);
;(function() {
    'use strict';
    angular.module('app.feedback')
    .config(Routes);
    
    function Routes($routeProvider) {
        
        $routeProvider.
        when('/feedback', {
            templateUrl: 'templates/feedback/views/feedback.client.view.html',
            controller: 'FeedbackController'
        });
        
    }
})();


;(function() {
    'use strict';
    
    angular.module('app.feedback')
    
    .controller('FeedbackController', FeedbackController);
    
    //PickAStudyController.$inject['$scope', '$routeParams', '$location', 'Authentication'];
     
    function FeedbackController($scope, $location, $rootScope, FeedbackRecordings, Audio, Recordings) {
        //laadt het menu
        $scope.user = $rootScope.user
        $scope.menuTemplate = "templates/partials/menu.html";
                
        $scope.newFeedback = {};
        $scope.newFeedback.body = "";
        
        $('.feedbackRecording-selected').hide();
        $scope.showDetails = function(feedbackrecording){
            //wanneer de gebruiker verschillende opnames aanklikt,
            //verdwijnt de vorige, geen get() dus in scope wijzigen.
            updatePrevRecInScope();
            
            $scope.pickedFeedbackRecording = feedbackrecording;   
            
            //heb je al feedback gegeven? Mag maar 1x
            $scope.givenFeedback = false;
            console.log($scope.pickedFeedbackRecording.feedbacks);

            $scope.pickedFeedbackRecording.feedbacks.forEach(function(feedback){
                console.log(feedback);
                if(feedback.username == $scope.user.username){
                    console.log('returned');
                    $scope.givenFeedback = true;
                }
            });
            
            //indien je geen feedback meer mag geven, verdwijnt form en wordt comments langer
            if($scope.pickedFeedbackRecording.username == $scope.user.username || $scope.givenFeedback == true){
                $('.comments').addClass('long');
            } else{
                $('.comments').removeClass('long');
            }
            
            $('audio').remove();
            
            Audio.placeTag('audio', $scope.pickedFeedbackRecording.data);
               
            $('.feedbackRecording-selected').show();
            $('.feedbackRecording-unselected').hide();
        }
        
        //CRUD
        $scope.find = function() {
            $scope.feedbackRecordings = FeedbackRecordings.query({
            }).$promise.then(function(recording){
                $scope.feedbackRecordings = recording;
                console.log(recording);
            });
        };
        
        $scope.addFeedback = function(){
            $scope.givenFeedback = true;
            $scope.newFeedback.username = $scope.user.username;
            $scope.newFeedback.likes = [];
            $scope.newFeedback.created_at = Date.now();
            $scope.newFeedback.approved = false;
            $scope.pickedFeedbackRecording.feedbacks.push($scope.newFeedback);
            Recordings.get({
                recordingId: $scope.pickedFeedbackRecording._id
            }).$promise.then(function(recording){
                recording.feedbacks.push($scope.newFeedback);
                recording.$update(function(){
                
                }, function(errorResponse) {
                    $scope.error = errorResponse.data.message;
                });
            });  
        }
        
        $scope.like = function(feedbackId){
            Recordings.get({
                recordingId: $scope.pickedFeedbackRecording._id
            }).$promise.then(function(recording){
                
                $scope.pickedFeedbackRecording.feedbacks.forEach(function(feedback) {
                    if(feedback._id == feedbackId){
                        feedback.likes.push($scope.user._id);
                    }
                });
                
                recording.feedbacks.forEach(function(feedback) {
                    if(feedback._id == feedbackId){
                        feedback.likes.push($scope.user._id);
                    }
                });
                recording.$update(function(){
                    
                }, function(errorResponse) {
                    $scope.error = errorResponse.data.message;
                });
            });
        }
        $scope.approve = function(feedbackId){
            Recordings.get({
                recordingId: $scope.pickedFeedbackRecording._id
            }).$promise.then(function(recording){
                $scope.pickedFeedbackRecording.approvedFeedbacks++;
                $scope.pickedFeedbackRecording.feedbacks.forEach(function(feedback) {
                    if(feedback._id == feedbackId){
                        feedback.approved = true;
                    }
                });
                
                recording.approvedFeedbacks++;
                if(recording.approvedFeedbacks >= 3){
                    removeFromFeedbackScope(recording);
                }
                recording.feedbacks.forEach(function(feedback) {
                    if(feedback._id == feedbackId){
                        feedback.approved = true;
                    }
                });
                recording.$update(function(){
                    
                }, function(errorResponse) {
                    $scope.error = errorResponse.data.message;
                });
            });
        }
        
        function updatePrevRecInScope(){
            $scope.feedbackRecordings.forEach(function(recording){
                if($scope.pickedFeedbackRecording){
                    if(recording._id == $scope.pickedFeedbackRecording._id){
                        recording = $scope.pickedFeedbackRecording;
                    }
                }
            })
        }
        
        function removeFromFeedbackScope(currentRecording){
            $('.feedbackRecording-selected').hide();
            $('.feedbackRecording-unselected').show();
            
            var index = 0;
            $scope.feedbackRecordings.forEach(function(recording){
                if(recording._id == currentRecording._id){
                    $scope.feedbackRecordings.splice(index, 1);
                }
                index++;
            })
        }
    }
    
})();

;(function() {
    'use strict';
    
    angular.module('app.feedback')
    
    .factory('FeedbackRecordings', FeedbackRecordings)
    .factory('Recordings', Recordings);
    
    //Recordings.$inject['$resource'];
    
    function FeedbackRecordings($resource) {
        return $resource('api/feedback-recordings/:feedbackId', {
        });
    }
    function Recordings($resource){
        return $resource('api/recordings/:recordingId', {
            recordingId: '@_id'
        }, {
            update: {
                method: 'PUT'
            }
        });
    }
    
})();

;(function() {
    'use strict';
    angular.module('app.studies')
    .config(Routes);
    
    //Routes.$inject['$routeProvider'];
    
    function Routes($routeProvider) {
        
        $routeProvider.
        when('/pick-note', {
            templateUrl: 'templates/learn-note/views/pick-note.client.view.html',
            controller: 'PickANoteController'
        }).
        when('/learn-note/:noteId', {
            templateUrl: 'templates/learn-note/views/learn-note.client.view.html',
            controller: 'LearnANoteController'
        });
    }
})();

;(function() {
    'use strict';
    
    angular.module('app.notes')    
    .controller('LearnANoteController', LearnANoteController);
        
    //PickANoteController.$inject['$scope', '$routeParams', '$location', 'Notes', '$rootScope'];
    
    function LearnANoteController($scope, $routeParams, $location, Notes, $rootScope, Users, Pitcher, Gamification, NoteService){
        //laadt het menu
        $scope.user = $rootScope.user;
        $scope.menuTemplate = "templates/partials/menu.html";
              
        //CRUD
        var note;
        
        $scope.findOne = function() {
            $scope.note = Notes.get({
                noteId: $routeParams.noteId
            }).$promise.then(function(data){
                $scope.tips = data.tips;
                if(NoteService.CheckIfNoteLearned(data, $scope.user) == true){
                    $('h4.rulesLearnNote').text("Je hebt de " + data.name.toLowerCase() + " al aangeleerd, goed gedaan!");
                    $('p.rulesLearnNote').text("Je kan de noot nog steeds oefenen maar er zal niet meer afgeteld worden.");
                }
                setScoreAndListen(data);})};
            
        function setScoreAndListen(response){
            note = response;
            $('.exampleNoteDiv h2').html(note.name);
            Pitcher.fillExampleField(note, "#exampleNote");
            $('img.example').attr('src', note.finger);
            
            //vraag toestemming voor de microfoon
            navigator.mediaDevices.getUserMedia({audio: true, video: false})
            .then(function(audioStream){
                $scope.stream = audioStream;
                reviseAndCountDown(audioStream)});}
                
        function reviseAndCountDown(mediaStream){
            //luister en tel af
            Pitcher.noteToLearn(note);
            Pitcher.gotStream(mediaStream);
            Pitcher.updatePitch(note);
            if (!NoteService.CheckIfNoteLearned(note, $scope.user) == true){
                Pitcher.myVar(setInterval(
                    reviser, 1000));}}
        
        function reviser(){
            Pitcher.meetDoel()
                .then(function(){  
                    fillUserData();});}
        
        function fillUserData(){
            $rootScope.learnedNote = note.name;
            $scope.user.learnedNotes.push(note._id);
            $scope.user = Gamification.checkForLevelUp($scope.user, note.points)
            .then(function(updatedUser){
                console.log('after gamification check');
                console.log(updatedUser);
                updateUserAndGoHome(updatedUser);});}
        
        function updateUserAndGoHome(updatedUser){
            Users.get({
                userId: $rootScope.user._id
            }).$promise.then(function(user){
                user.profile = updatedUser.profile;
                user.level = updatedUser.level;
                user.learnedNotes.push(note._id);
                $rootScope.user = updatedUser;
                user.$update(function() {
                        $location.path('/');
                    }, function(errornote) {
                        $scope.error = errornote.data.message;
                    }
                )}
            );
        }
    }
    
})();   
;(function() {
    'use strict';
    
    angular.module('app.notes')
    
    .controller('PickANoteController', PickANoteController);
    
    //PickANoteController.$inject['$scope', '$routeParams', '$location', 'Notes', '$rootScope'];
    
    function PickANoteController($scope, $routeParams, $location, Notes, $rootScope, RankedNotes, NoteService, Pitcher) {
        //laadt het menu
        $scope.user = $rootScope.user;
        $scope.menuTemplate = "templates/partials/menu.html";

        //hover om de noot op de notenbalk te zien
        $scope.showNote = function(note, field){
                $(field).prev().hide();
                Pitcher.fillExampleField(note, field);
            } 
        $scope.clearField = function(canvas){
            $('#' + canvas).prev().show();
            canvas = document.getElementById(canvas);
            var context = canvas.getContext("2d");
            context.clearRect(0, 0, canvas.width, canvas.height);
        }
        //heb je de noot al geleerd of niet?
        $scope.checkNote = function(note){
            return NoteService.CheckIfNoteLearned(note, $scope.user);
        }
        
        //CRUD
        var rank = $scope.user.rank.value + 1;
        $scope.find = function() {
            $scope.notes = RankedNotes.query({
                rank: rank
            });
        };
        
        
    }
    
})();

;(function() {
    'use strict';
    
    angular.module('app.notes')
    .factory('RankedNotes', RankedNotes);
    
    function RankedNotes($resource) {
        return $resource('api/notes-rank/:rank', {
            rank: '@rank'
        });
    }
    
})();
;(function() {
    'use strict';
    angular.module('app.notes')
    .factory('Pitcher', Pitcher);
    
    function Pitcher($q, ngDialog){
        var audioContext = new AudioContext(),
            MAX_SIZE = Math.max(4,Math.floor(audioContext.sampleRate/5000)), // corresponds to a 5kHz signal
            isPlaying = false,
            analyser = null, 
            mediaStreamSource, //Moet hier, want anders komt het te vroeg in de garbage collector waardoor het stopt met luisteren
            pitch,
            noteToLearn,
            publicNote = "Do", //de noot die gespeeld wordt
            myVar, //timer
            ac, //is er een noot?
            octaaf, //octaaf van de gespeelde noot (alt != sopraan)
            buf = new Float32Array( 1024 ),
            countDown = 5;    
            
        var service = {
            noteToLearn: noteToLearnGS,
            myVar: myVarGS,
          
            fillExampleField: fillExampleField,   
            gotStream: gotStream,
            updatePitch: updatePitch,
            meetDoel: meetDoel
        };
        
        return service;
                
        //=============================
        //DECLARE VARIABLES
        //=============================
        window.AudioContext = window.AudioContext || window.webkitAudioContext;

        
        
        function noteToLearnGS(note){
            noteToLearn = note;
        }
        function myVarGS(vari){
            myVar = vari;
        }
        
        //=============================
        //PUBLIC FUNCTIONS
        //=============================
            
        function fillExampleField(note, canvas) {
            var exampleCanvas = $(canvas)[0];
            var exampleRenderer = new Vex.Flow.Renderer(exampleCanvas, Vex.Flow.Renderer.Backends.CANVAS);
            var exampleContext = exampleRenderer.getContext();
            var exampleStave = new Vex.Flow.Stave(10, 0, 150);
            
            exampleStave.addClef("treble").setContext(exampleContext).draw();

            var key = note.key + '/' + note.octave;

            var exampleNote = [
                new Vex.Flow.StaveNote({ keys: [key], duration: "q"})
            ];

            //kruis of mol toevoegen indien nodig
            if(note.key.length == 2){
                var accidental = note.key.substr(note.key.length - 1);
                exampleNote[0].addAccidental(0, new Vex.Flow.Accidental(accidental));
            }
            
            //een maat moet steeds vol zijn, voor 1 kwartnoot dus: 1 beat met value 4(kwartnoot)
            var exampleVoice = new Vex.Flow.Voice({
                num_beats: 1,
                beat_value: 4,
                resolution: Vex.Flow.RESOLUTION
            });

            exampleVoice.addTickables(exampleNote);

            var formatter = new Vex.Flow.Formatter().
                joinVoices([exampleVoice]).format([exampleVoice], 250);

            exampleVoice.draw(exampleContext, exampleStave);
        }

        function gotStream(stream) {
            // Create an AudioNode from the stream.
            mediaStreamSource = audioContext.createMediaStreamSource(stream);

            //The createAnalyser() method of the AudioContext interface creates an AnalyserNode, 
            //which can be used to expose audio time and frequency data and create data visualisations.
            analyser = audioContext.createAnalyser();
            analyser.fftSize = 2048;
            //Connect the two
            mediaStreamSource.connect( analyser );   
        }

        function updatePitch() {
            
            var noteElem = $( "#notePlayed" );

            analyser.getFloatTimeDomainData( buf );
            ac = autoCorrelate( buf, audioContext.sampleRate ); //autoCorrelate filtert ontoevalligheden

            //er is geen noot
            if (ac == -1) {
                noteElem.innerText = "...";
            }//er is een noot
            else {
                //frequentie
                pitch = ac;
                
                //NOOTNAAM IN HTML
                //note is een afgerond getal. deel door 12 om de correcte index van notestrings te bekomen (er zijn nml 12 noten)
                var noteStrings = ["Do", "Do#", "Re", "Mib", "Mi", "Fa", "Fa#", "Sol", "Sol#", "La", "Sib", "Si"];
                var note =  noteFromPitch( pitch ); 
                noteElem.html(noteStrings[note%12].toLocaleLowerCase());
                publicNote = noteElem.innerHTML = noteStrings[note%12];

                setOctave(note); //bereken de var octaaf
                
                drawPlayedNote();
            }

            //zorgt ervoor dat updatePitch telkens opnieuw uitgevoerd wordt en het canvas opnieuw getekend
            if (!window.requestAnimationFrame)
                window.requestAnimationFrame = window.webkitRequestAnimationFrame;
            var rafID = window.requestAnimationFrame( updatePitch );
        }


        function meetDoel(){
            return $q( function(resolve, reject) {
                var htmlBlock = $("#countDown");
                var htmlText = $("#countDown h2");
                htmlText.html(countDown);
                //De juiste noot wordt gespeeld
                if(publicNote.toLowerCase() == noteToLearn.name.toLowerCase() && ac !== -1 && octaaf == noteToLearn.octave){
                    htmlBlock.css("display", "block");
                    if(countDown == 0){
                        ngDialog.open({ template: 'templates/partials/noteLearned.html', className: 'ngdialog-theme-default' });
                        window.clearInterval(myVar);
                        resolve('noot geleerd');
                    }else{
                        htmlText.innerHTML = countDown;
                        countDown = countDown - 1;
                        reject('noot niet lang genoeg aangehouden');
                    }
                // De foute noot wordt gespeeld
                }else{
                    countDown = 5;
                    htmlBlock.css("display", "none");
                    reject('de foute noot gespeeld');
                }
                
            });
        };
        
        //=============================
        //LOCAL FUNCTIONS
        //=============================

        function noteFromPitch( frequency ) {
            var noteNum = 12 * (Math.log( frequency / 440 )/Math.log(2) );
            return Math.round( noteNum ) + 69;
        }

        //100 cent is een halve noot
        //https://nl.wikipedia.org/wiki/Cent_%28muziek%29
        //formule die hier gebruikt wordt is een algemene formule die hierboven te vinden is
        function centsOffFromPitch( frequency, note ) {
            return Math.floor( 1200 * Math.log( frequency / frequencyFromNoteNumber( note ))/Math.log(2) );
        }

        function frequencyFromNoteNumber( note ) {
            return 440 * Math.pow(2,(note-69)/12);
        }

        function setOctave(note) {

            switch(true){
                    case (note>=60 && note<72):
                        octaaf = 3;
                        break;
                    case (note>=72 && note<84):
                        octaaf = 4;
                        break;
                    case (note>=84 && note<96):
                        octaaf = 5;
                        break;
                    case (note>=96 && note<108):
                        octaaf = 6;
                        break;
                }

                //Bij alt wordt ocaaf + 1 want die klinkt lager voor dezelfde noot
                if(noteToLearn.treble){
                    ++octaaf;
                    }
        }

        function drawPlayedNote(){
            
                var exerciseCanvas = $("#exerciseNote")[0];
                var exerciseVoice = new Vex.Flow.Voice({
                    num_beats: 1,
                    beat_value: 4,
                    resolution: Vex.Flow.RESOLUTION
                });
                var exerciseRenderer = new Vex.Flow.Renderer(exerciseCanvas, Vex.Flow.Renderer.Backends.CANVAS);
                var exerciseContext = exerciseRenderer.getContext();
                exerciseContext.clearRect(0, 0, exerciseCanvas.width, exerciseCanvas.height);
                var exerciseStave = new Vex.Flow.Stave(10, 0, 150);
                exerciseStave.addClef("treble").setContext(exerciseContext).draw();
                var noteArray = null;

                noteArray = getVexFlowNote();

                exerciseVoice.addTickables(noteArray);
                var formatter = new Vex.Flow.Formatter().joinVoices([exerciseVoice]).format([exerciseVoice], 250);
                exerciseVoice.draw(exerciseContext, exerciseStave);
                
        }

        function getVexFlowNote(){
            var noteArray;
            
            switch(publicNote){
                    case "Do":
                        noteArray = [ new Vex.Flow.StaveNote({ keys: ["c/" + octaaf], duration: "q"}) ];
                        break;
                    case "Do#":
                        noteArray = [ new Vex.Flow.StaveNote({ keys: ["c#/" + octaaf], 
                        duration: "q"}).addAccidental(0, new Vex.Flow.Accidental("#")) ];
                        break;
                    case "Re":
                        noteArray = [ new Vex.Flow.StaveNote({ keys: ["d/" + octaaf], duration: "q"}) ];
                        break;
                    case "Mib":
                        noteArray = [ new Vex.Flow.StaveNote({ keys: ["eb/" + octaaf], duration: "q"}).addAccidental(0, new Vex.Flow.Accidental("b")) ];
                        break;
                    case "Mi":
                        noteArray = [ new Vex.Flow.StaveNote({ keys: ["e/" + octaaf], duration: "q"}) ];
                        break;
                    case "Fa":
                        noteArray = [ new Vex.Flow.StaveNote({ keys: ["f/" + octaaf], duration: "q"}) ];
                        break;
                    case "Fa#":
                        noteArray = [ new Vex.Flow.StaveNote({ keys: ["f#/" + octaaf], duration: "q"}).addAccidental(0, new Vex.Flow.Accidental("#")) ];
                        break;
                    case "Sol":
                        noteArray = [ new Vex.Flow.StaveNote({ keys: ["g/" + octaaf], duration: "q"}) ];
                        break;
                    case "Sol#":
                        noteArray = [ new Vex.Flow.StaveNote({ keys: ["g#/" + octaaf], duration: "q"}).addAccidental(0, new Vex.Flow.Accidental("#")) ];
                        break;
                    case "La":
                        noteArray = [ new Vex.Flow.StaveNote({ keys: ["a/" + octaaf], duration: "q"}) ];
                        break;
                    case "Sib":
                        noteArray = [ new Vex.Flow.StaveNote({ keys: ["bb/" + octaaf], duration: "q"}).addAccidental(0, new Vex.Flow.Accidental("b")) ];
                        break;
                    case "Si":
                        noteArray = [ new Vex.Flow.StaveNote({ keys: ["b/" + octaaf], duration: "q"}) ];
                        break;
                    default :
                        noteArray = [ new Vex.Flow.StaveNote({ keys: ["g/" + octaaf], duration: "q"}) ];
                        break;
                }
                
            return noteArray;
        }

        //toevalligheden
        function autoCorrelate( buf, sampleRate ) {
            var SIZE = buf.length;
            var MAX_SAMPLES = Math.floor(SIZE/2);
            var best_offset = -1;
            var best_correlation = 0;
            var rms = 0;
            var foundGoodCorrelation = false;
            var correlations = new Array(MAX_SAMPLES);
            var MIN_SAMPLES = 0;  // will be initialized when AudioContext is created.

            for (var i=0;i<SIZE;i++) {
                var val = buf[i];
                rms += val*val;
            }
            rms = Math.sqrt(rms/SIZE);
            if (rms<0.01) // not enough signal
                return -1;

            var lastCorrelation=1;
            for (var offset = MIN_SAMPLES; offset < MAX_SAMPLES; offset++) {
                var correlation = 0;

                for (var i=0; i<MAX_SAMPLES; i++) {
                    correlation += Math.abs((buf[i])-(buf[i+offset]));
                }
                correlation = 1 - (correlation/MAX_SAMPLES);
                correlations[offset] = correlation; // store it, for the tweaking we need to do below.
                if ((correlation>0.9) && (correlation > lastCorrelation)) {
                    foundGoodCorrelation = true;
                    if (correlation > best_correlation) {
                        best_correlation = correlation;
                        best_offset = offset;
                    }
                } else if (foundGoodCorrelation) {
                    // short-circuit - we found a good correlation, then a bad one, so we'd just be seeing copies from here.
                    // Now we need to tweak the offset - by interpolating between the values to the left and right of the
                    // best offset, and shifting it a bit.  This is complex, and HACKY in this code (happy to take PRs!) -
                    // we need to do a curve fit on correlations[] around best_offset in order to better determine precise
                    // (anti-aliased) offset.

                    // we know best_offset >=1,
                    // since foundGoodCorrelation cannot go to true until the second pass (offset=1), and
                    // we can't drop into this clause until the following pass (else if).
                    var shift = (correlations[best_offset+1] - correlations[best_offset-1])/correlations[best_offset];
                    return sampleRate/(best_offset+(8*shift));
                }
                lastCorrelation = correlation;
            }
            if (best_correlation > 0.01) {
                // console.log("f = " + sampleRate/best_offset + "Hz (rms: " + rms + " confidence: " + best_correlation + ")")
                return sampleRate/best_offset;
            }
            return -1;
        //	var best_frequency = sampleRate/best_offset;
        }
    }
    
})();
;(function() {
    'use strict';
    angular.module('app.recordings')
    .config(Routes);
    
    function Routes($routeProvider) {
        
        $routeProvider.
        //when no recording is selected
        when('/recordings', {
            templateUrl: 'templates/recordings/views/recordings.client.view.html',
            controller: 'RecordingsController'
        })
        //when a recording is selected and we want to update,
        //we need the id in the url
        .when('/recordings/:recordingId', {
            templateUrl: 'templates/recordings/views/recordings.client.view.html',
            controller: 'RecordingsController'
        });
        
    }
})();


;(function() {
    'use strict';
    
    angular.module('app.recordings')
    
    .controller('RecordingsController', RecordingsController);
    
    //PickAStudyController.$inject['$scope', '$routeParams', '$location', 'Authentication'];
     
    function RecordingsController($scope, $location, $rootScope, RecordingsUser, Recordings, $routeParams, Audio) {
        //laadt het menu
        $scope.user = $rootScope.user
        $scope.menuTemplate = "templates/partials/menu.html";
        
            
        
        $scope.goToRecording = function ( id ) {
            $location.path( 'recordings/' + id );
        };
        
        $('.study-selected').hide();
        $scope.showDetails = function(recording){
            $scope.pickedRecording = recording;
            $scope.pickedRecording.numberFeedbacks = Object.keys(recording.feedbacks).length;
            console.log(  $scope.pickedRecording.numberFeedbacks);
            $('audio').remove();
            
            Audio.placeTag('audio', $scope.pickedRecording.data);
            
            $('.study-selected').show();
            $('.study-unselected').hide();
        }
        
        //CRUD
        $scope.find = function() {
            $scope.recordings = RecordingsUser.query({
                userId: $scope.user._id
            }).$promise.then(function(recordings){
                $scope.recordings = recordings;
            });
        };
        $scope.placeInFeedback = function() {
            Recordings.get({
                recordingId: $scope.pickedRecording._id
            }).$promise.then(function(recording){
                recording.inFeedback = true;
                recording.$update(function(){
                    $scope.pickedRecording.inFeedback = true;
                });

            });
        }
    }
    
})();

;(function() {
    'use strict';
    angular.module('app.notes')
    .factory('Audio', Audio);
    
    function Audio($sce){
       
            
        var service = {
            placeTag: placeTag
        };
        
        return service;
       
       function placeTag(elem, audioSrc){
            var audioElem = document.createElement("audio");
            audioElem.setAttribute("controls", "controls");
            var src = document.createElement("source");
            src.setAttribute('src', trustAudio(audioSrc));
            src.setAttribute('type', 'audio/ogg');
            audioElem.appendChild(src);
            var element = document.getElementById(elem);
            element.appendChild(audioElem); 
       }
       
        function trustAudio(audio) {
            return $sce.trustAsResourceUrl(audio);
        }
    }
    
})();
;(function() {
    'use strict';
    
    angular.module('app.recordings')
    .factory('RecordingsUser', RecordingsUser)
    .factory('Recordings', Recordings);
    
    //Recordings.$inject['$resource'];
    
    function RecordingsUser($resource) {
        return $resource('api/recordings-user/:userId', {
            userId: '@userId'
        },             
        
        {'query':  {method:'GET', isArray:true},
        });
    }
    function Recordings($resource) {
        return $resource ('api/recordings/:recordingId', {
            recordingId: '@recordingId'
        }, {
            update: {
                method: 'PUT'
            }
        });
    }
    
})();

;(function() {
    'use strict';
    angular.module('app.studies')
    .config(Routes);
    
    function Routes($routeProvider) {
        
        $routeProvider.
        when('/pick-study', {
            templateUrl: 'templates/studies/views/pick-study.client.view.html',
            controller: 'PickAStudyController'
        }).
        when('/study/:studyId', {
            templateUrl: 'templates/studies/views/study.client.view.html',
            controller: 'StudyController'
        });
        
    }
})();


;(function() {
    'use strict';
    
    angular.module('app.studies')
    
    .controller('PickAStudyController', PickAStudyController);
    
    //PickAStudyController.$inject['$scope', '$routeParams', '$location', 'Authentication'];
     
    function PickAStudyController($scope, $routeParams, $location, $rootScope, RankedStudies) {
        //laadt het menu
        $scope.user = $rootScope.user
        $scope.menuTemplate = "templates/partials/menu.html";
        $('.study-selected').hide();
        $scope.showDetails = function(study){
            $scope.pickedStudy = study;
            $('.study-selected').show();
            $('.study-unselected').hide();
        }

        //CRUD
        var rank = $scope.user.rank.value + 1;
        $scope.find = function() {
            $scope.studies = RankedStudies.query({
                rank: rank
            });
        };
    }
    
})();

;(function() {
    'use strict';
    
    angular.module('app.studies')
    
    .controller('StudyController', StudyController);
    
    //PickAStudyController.$inject['$scope', '$routeParams', '$location', 'Authentication'];
     
    function StudyController($scope, $routeParams, $location, $rootScope, Studies, ScorePursuit, ngDialog, Recordings, Gamification, Users) {
        //laadt het menu
        $scope.user = $rootScope.user
        $scope.menuTemplate = "templates/partials/menu.html";
        
        //CRUD
        $scope.findOne = function(){
            $scope.study = Studies.get({
                studyId: $routeParams.studyId
            }).$promise.then(function(study){
                $scope.study = study;
                activate(study);
            });
        }
        
        var recordRTC;
        
        function activate(study){
            var notes = study.notes;
            var key = study.key;
            var time = study.time;
            
            
            
            ScorePursuit.dialogIsClosed();
            ScorePursuit.drawScore(notes, key, time);
            listen();
        }

        function listen(){
            navigator.mediaDevices.getUserMedia({audio: true, video: false})
            .then(function(audioStream){
                var options = {
                        mimeType: 'audio/ogg'
                    };
                recordRTC = RecordRTC(audioStream, options);
                $scope.startRecording = function(){
                    recordRTC.startRecording();
                    console.log('started recording');
                }                
                
                $scope.stopRecording = stopRecording;
                $scope.stream = audioStream;
                
                getReadyForPursuit(audioStream)});}
                
        function getReadyForPursuit(mediaStream){
            ScorePursuit.gotStream(mediaStream);
            ScorePursuit.updatePitch(recordRTC)
            .then(function(data){console.log(data)})
        }
        
        var stopRecording = function(stream){
            $scope.stream.stop();
            
            $scope.totalNotes = ($scope.study.notes.match(/\//g) || []).length;
            $scope.correctNotes = $scope.totalNotes - parseInt($('#fouteNoten').text());
            
            $scope.totalPointsNotes = $scope.study.checklist.notes;
            $scope.pointsNotes = Math.round($scope.correctNotes / $scope.totalNotes * $scope.totalPointsNotes);
                
            recordRTC.stopRecording(function(audioURL) {
                //audio.src = audioURL;
                
                $rootScope.audio = audioURL;
                
                recordRTC.getDataURL(function(dataURL) { 
                    $rootScope.audioURL = dataURL;
                });
            });
            
            ngDialog.openConfirm({
                template: 'templates/partials/studyFinished.html',
                className: 'ngdialog-theme-default',
                scope: $scope, // <- ability to use the scopes from directive controller
            }).then(function (success) {
                console.log('close with save');
                $scope.studyDescription = $('#studyDescription').val();
                
                recordRTC.getDataURL(function(dataURL) { 
                    var recording = new Recordings({
                        rank: $scope.study.rank,
                        title: $scope.study.name,
                        description: $scope.studyDescription,
                        author: $scope.study.author,
                        user :  $scope.user._id,
                        username: $scope.user.username,
                        data: dataURL,
                        testChecklist : $scope.study.testChecklist
                    });

                    recording.$save(function(response){
                        $location.path('/');
                    }, function (errorResponse) {
                        console.log(errorResponse);
                    });
                    
                });
            }, function (error) {
                $location.path('/');
                console.log('close without save');
            });
        }

        $rootScope.$on('ngDialog.opened', function (e, $dialog) {
            $('#study-dialog audio').remove();
            
            var audio = document.createElement("audio");
            audio.setAttribute("controls", "controls");
            var src = document.createElement("source");
            src.setAttribute('src', $rootScope.audio);
            src.setAttribute('type', 'audio/ogg');
            audio.appendChild(src);
            var elem = document.getElementById('study-dialog');
            elem.appendChild(audio);
            
            audio.addEventListener('loadedmetadata', function() {
                
                var duration = audio.duration;
                var perfectDuration = $scope.study.perfectDuration;
                console.log(duration);
                console.log(perfectDuration);
                //aantal seconden ernaast (niet letterlijk want aangepast)
                var missedSeconds = Math.abs(perfectDuration - duration);
                
                $scope.percentageDuration = Math.round(100 - missedSeconds * 4);
                $scope.totalPointsTempo = $scope.study.checklist.tempo;
                $scope.pointsTempo = Math.round($scope.percentageDuration / 100 * $scope.totalPointsTempo);
                
                $scope.earnedPercentage = ($scope.pointsNotes + $scope.pointsTempo) / 40 * 100;
                $scope.earnedPoints = Math.round($scope.study.maxPoints * ($scope.earnedPercentage/100));
                
                $scope.$apply();
                
                $scope.user = Gamification.checkForLevelUp($scope.user, $scope.earnedPoints)
                .then(function(updatedUser){
                    console.log(updatedUser);
                    $scope.user = updatedUser;
                    updateUserAndGoBack(updatedUser);
                });           
            });
        });
        
        function updateUserAndGoBack(updatedUser){
            Users.get({
                userId: $rootScope.user._id
            }).$promise.then(function(user){
                user.profile = updatedUser.profile;
                user.level = updatedUser.level;
                
                user.$update(function() {
                    }, function(errornote) {
                        $scope.error = errornote.data.message;
                    }
                )}
                
            );
       }
    
        $scope.setActive = function(event){
            $('.instrument li.active').removeClass("active");
            $(event.target).addClass('active');
        }
    }
})();

;(function() {
    'use strict';
    
    angular.module('app.studies')
    
    .factory('Recordings', Recordings);
    //.factory('RankedStudies', RankedStudies);
    
    //Studies.$inject['$resource'];
    
    function Recordings($resource) {
        return $resource('api/recordings/:recordingId', {
            recordingId: '@_id'
        });
    }
    
    // function RankedStudies($resource) {
    //     return $resource('api/studies-rank/:rank', {
    //         rank: '@rank'
    //     });
    // }
    
})();

;(function() {
    'use strict';
    angular.module('app.studies')
    .factory('ScorePursuit', ScorePursuit);
    
    function ScorePursuit($q, ngDialog){
        var audioContext = new AudioContext(),
            MAX_SIZE = Math.max(4,Math.floor(audioContext.sampleRate/5000)), // corresponds to a 5kHz signal
            isPlaying = false,
            analyser = null, 
            mediaStreamSource, //Moet hier, want anders komt het te vroeg in de garbage collector waardoor het stopt met luisteren
            pitch,
            noteToLearn,
            publicNote = "Do", //de noot die gespeeld wordt
            myVar, //timer
            ac, //is er een noot?
            octaaf, //octaaf van de gespeelde noot (alt != sopraan)
            buf = new Float32Array( 1024 ),
            countDown = 5,
            publicScore,
            publicKey,
            publicTime,
            finished = false,
            dialogClosed = true,
            noteBefore = "x";    
            
        var service = {
            noteToLearn: noteToLearnGS,
            myVar: myVarGS,
            dialogIsClosed : dialogIsClosed,
          
            drawScore: drawScore,   
            gotStream: gotStream,
            updatePitch: updatePitch,
            meetDoel: meetDoel
        };
        
        return service;
                
        //=============================
        //DECLARE VARIABLES
        //=============================
        window.AudioContext = window.AudioContext || window.webkitAudioContext;

        
        
        function noteToLearnGS(note){
            noteToLearn = note;
        }
        function myVarGS(vari){
            myVar = vari;
        }
        function dialogIsClosed(){
            dialogClosed = true;
            finished = false;
        }
        
        //=============================
        //PUBLIC FUNCTIONS
        //=============================
            
        function drawScore(notes, key, time) {
            publicScore = notes;
            publicKey = key;
            publicTime = time;
            var vextab = VexTabDiv;
            var VexTab = vextab.VexTab;
            var Artist = vextab.Artist;
            var Renderer = Vex.Flow.Renderer;
            
            // Create VexFlow Renderer from canvas element with id #boo
            var renderer = new Renderer($('#score')[0], Renderer.Backends.CANVAS);
        
            var width = document.getElementsByClassName('study-container')[0].clientWidth - 40;
            // Initialize VexTab artist and parser.
            var artist = new Artist(10, 10, width, {scale: 1});
            vextab = new VexTab(artist);

            try {
                
                //je hebt notes met alle noten in
                //knip hieruit de eerste 25 (indien er 25 zijn)
                //plak achteraan de uiteindelijke string    
                var twentyFiveNotes;
                var finalString = "";
                var countNotes = (notes.match(/\//g) || []).length;
                var lineIndex = 1;
                while(countNotes > 25){
                    var position = nth_occurrence(notes, '/', 25);
                    var index = 0;
                    while(notes.length > index){
                        if(notes.slice(position,position + 1) !== '|')
                        {
                            position--;
                        }
                        else{
                            position++;
                            break;
                        }
                        index++;
                    }
                    twentyFiveNotes = notes.substring(0, position);
                    notes = notes.replace(twentyFiveNotes, "");
                    finalString = finalString.concat("\n tabstave notation=true tablature=false time=" + time + " key=" + key + "\n notes " + twentyFiveNotes.trim() + " ")
                    countNotes = (notes.match(/\//g) || []).length;
                    if(lineIndex >= 3 && twentyFiveNotes.indexOf('$.top.$ $●$') >= 0){
                        // var div = document.getElementsByClassName('score')[0];
                        // div.scrollTop = 100 * (lineIndex - 2);
                        var scroll = 100 * (lineIndex - 2);
                        console.log(scroll);
                        $('div.score').stop(true,true).animate({ scrollTop: scroll}, 5000, "swing");
                        console.log('we zitten op lijn ' + lineIndex);
                    }
                    
                    lineIndex++;
                }
                finalString = finalString.concat("\n tabstave notation=true tablature=false time=" + time + " key=" + key + "\n notes " + notes.trim());
                
                //indien dit boven een bepaald aantal is
                // neem de nde figuur en zoek naar de | ervoor
                // voeg hierna toe: "\n tabstave ....\n notes"
                // dit doe je opnieuw voor de notes op de laatste rij
                
                // Parse VexTab music notation passed in as a string.
                vextab.parse(finalString + "=|=");

                // Render notation onto canvas.
                artist.render(renderer);
            } catch (e) {
                console.log(e);
            }
            
        }

        function gotStream(stream) {
            // Create an AudioNode from the stream.
            mediaStreamSource = audioContext.createMediaStreamSource(stream);

            //The createAnalyser() method of the AudioContext interface creates an AnalyserNode, 
            //which can be used to expose audio time and frequency data and create data visualisations.
            analyser = audioContext.createAnalyser();
            analyser.fftSize = 2048;
            //Connect the two
            mediaStreamSource.connect( analyser );   
        }

        function updatePitch() {
            return $q( function(resolve, reject) {
                analyser.getFloatTimeDomainData( buf );
                ac = autoCorrelate( buf, audioContext.sampleRate ); //autoCorrelate filtert ontoevalligheden

                //er is geen noot
                if (ac == -1) {
                    if(finished == true && dialogClosed == true){
                        //we willen van hier een functie uit de controller triggeren
                        $("#stopRecording").trigger("click");
                        
                        dialogClosed = false;
                    }
                    reject('geen geluid');
                }//er is een noot
                else {
                    //frequentie
                    pitch = ac;
                    
                    //NOOTNAAM IN HTML
                    //note is een afgerond getal. deel door 12 om de correcte index van notestrings te bekomen (er zijn nml 12 noten)
                    var noteStrings = ["C", "C#", "D", "E@", "E", "F", "F#", "G", "G#", "A", "B@", "B"];
                    var note =  noteFromPitch( pitch ); 

                    setOctave(note); //bereken de var octaaf
                    resolve(noteStrings[note%12] + '/' + octaaf);
                    
                    pursuit(publicScore, noteStrings[note%12] + '/' + octaaf);
                }
                    //zorgt ervoor dat updatePitch telkens opnieuw uitgevoerd wordt en het canvas opnieuw getekend
                    if (!window.requestAnimationFrame)
                        window.requestAnimationFrame = window.webkitRequestAnimationFrame;
                    var rafID = window.requestAnimationFrame( updatePitch );
                
            })
            
        }
        
        function pursuit(score, playedNote){
            var noteToPlay;
            var position;
            var noteAfter;
            //SET POSITION AND NOTE TO PLAY=============
            if(score.indexOf('$') == -1){//eerste noot is nog niet gespeeld
                noteToPlay = score.substring(0, 3); 
                position = 0;                
            }else{//eerste noot is gespeeld

                //waar we zitten in de partituur wordt aangegeven door deze string,
                //dit is het bolletje dat je ziet meebewegen (13 = lengte van deze string)
                position = score.indexOf("$.top.$ $●$") + 12;

                //de noot die we moeten spelen is dus na de positie te vinden
                noteToPlay = score.substring(position, position + 3);
            }
            
            var data = checkIfIsNote(noteToPlay, position, score);
            noteToPlay = data[0];
            position = data[1];
            
            var positionNoteAfter = position + 4;
            noteAfter = score.substring(positionNoteAfter, positionNoteAfter + 3);
            var dataNoteAfter = checkIfIsNote(noteAfter, positionNoteAfter, score);
            noteAfter = dataNoteAfter[0];
            positionNoteAfter = dataNoteAfter[1];
            
            var positionThirdNote = positionNoteAfter + 4;
            var thirdNote = score.substring(positionThirdNote, positionThirdNote + 3);
            var dataThirdNote = checkIfIsNote(thirdNote, positionThirdNote, score);
            thirdNote = dataNoteAfter[0];
            
            //CHECK IF THE PLAYED NOTE MATCHES THE NOTE TO PLAY AND FOLLOW
            //very first note, has to be correct
            if(noteToPlay == playedNote && score.indexOf('$') <= -1){
                $("#startRecording").trigger("click");
                score = score.slice(0, position + 3) + " $.top.$ $●$" + score.slice(position + 3);
                noteBefore = noteToPlay;
            }
            //playing the correct note
            else if(noteToPlay == playedNote && score.indexOf('$') > -1){
                score = score.replace(" $.top.$ $●$", "");
                score = score.slice(0, position - 9) + " $.top.$ $●$" + score.slice(position - 9);
                noteBefore = noteToPlay;
            }
            //played a wrong note
            else if(noteAfter == playedNote && score.indexOf('$') > -1 && thirdNote != noteBefore ){
                score = score.replace(" $.top.$ $●$", "");
                score = score.slice(0, positionNoteAfter - 9) + " $.top.$ $●$" + score.slice(positionNoteAfter - 9);
                noteBefore = noteAfter;
                var count = parseInt(document.getElementById('fouteNoten').innerHTML);
                count++;
                document.getElementById('fouteNoten').innerHTML = count;
            }
            if(position > score.length - 2){
                finished = true;
            }
                            
            drawScore(score, publicKey, publicTime);

        }
        
        function insertAtIndex(idx, rem, str) {
            return this.slice(0, idx) + str + this.slice(idx + Math.abs(rem));
        };

        function meetDoel(){
            return $q( function(resolve, reject) {
                var html = $("#countDown");
                html.html(countDown);
                //De juiste noot wordt gespeeld
                if(publicNote.toLowerCase() == noteToLearn.name && ac !== -1 && octaaf == noteToLearn.octave){
                    html.css("display", "block");
                    if(countDown == 0){
                        window.alert("Je kan een " + noteToLearn.name + " spelen, proficiat!");
                        window.clearInterval(myVar);
                        resolve('noot geleerd');
                    }else{
                        html.innerHTML = countDown;
                        countDown = countDown - 1;
                        reject('noot niet lang genoeg aangehouden');
                    }
                // De foute noot wordt gespeeld
                }else{
                    countDown = 5;
                    html.css("display", "none");
                    reject('de foute noot gespeeld');
                }
                
            });
        };
        
        //=============================
        //LOCAL FUNCTIONS
        //=============================

        function noteFromPitch( frequency ) {
            var noteNum = 12 * (Math.log( frequency / 440 )/Math.log(2) );
            return Math.round( noteNum ) + 69;
        }

        //100 cent is een halve noot
        //https://nl.wikipedia.org/wiki/Cent_%28muziek%29
        //formule die hier gebruikt wordt is een algemene formule die hierboven te vinden is
        function centsOffFromPitch( frequency, note ) {
            return Math.floor( 1200 * Math.log( frequency / frequencyFromNoteNumber( note ))/Math.log(2) );
        }

        function frequencyFromNoteNumber( note ) {
            return 440 * Math.pow(2,(note-69)/12);
        }

        function setOctave(note) {

            switch(true){
                    case (note>=60 && note<72):
                        octaaf = 3;
                        break;
                    case (note>=72 && note<84):
                        octaaf = 4;
                        break;
                    case (note>=84 && note<96):
                        octaaf = 5;
                        break;
                    case (note>=96 && note<108):
                        octaaf = 6;
                        break;
                }

                setOctaveToInstrument();

        }

        function drawPlayedNote(){
            
                var exerciseCanvas = $("#exerciseNote")[0];
                var exerciseVoice = new Vex.Flow.Voice({
                    num_beats: 1,
                    beat_value: 4,
                    resolution: Vex.Flow.RESOLUTION
                });
                var exerciseRenderer = new Vex.Flow.Renderer(exerciseCanvas, Vex.Flow.Renderer.Backends.CANVAS);
                var exerciseContext = exerciseRenderer.getContext();
                exerciseContext.clearRect(0, 0, exerciseCanvas.width, exerciseCanvas.height);
                var exerciseStave = new Vex.Flow.Stave(10, 0, 500);
                exerciseStave.addClef("treble").setContext(exerciseContext).draw();
                var noteArray = null;

                noteArray = getVexFlowNote();

                exerciseVoice.addTickables(noteArray);
                var formatter = new Vex.Flow.Formatter().joinVoices([exerciseVoice]).format([exerciseVoice], 250);
                exerciseVoice.draw(exerciseContext, exerciseStave);
                
        }

        function getVexFlowNote(){
            var noteArray;
            
            switch(publicNote){
                    case "Do":
                        noteArray = [ new Vex.Flow.StaveNote({ keys: ["c/" + octaaf], duration: "q"}) ];
                        break;
                    case "Do#":
                        noteArray = [ new Vex.Flow.StaveNote({ keys: ["c#/" + octaaf], 
                        duration: "q"}).addAccidental(0, new Vex.Flow.Accidental("#")) ];
                        break;
                    case "Re":
                        noteArray = [ new Vex.Flow.StaveNote({ keys: ["d/" + octaaf], duration: "q"}) ];
                        break;
                    case "Mib":
                        noteArray = [ new Vex.Flow.StaveNote({ keys: ["eb/" + octaaf], duration: "q"}).addAccidental(0, new Vex.Flow.Accidental("b")) ];
                        break;
                    case "Mi":
                        noteArray = [ new Vex.Flow.StaveNote({ keys: ["e/" + octaaf], duration: "q"}) ];
                        break;
                    case "Fa":
                        noteArray = [ new Vex.Flow.StaveNote({ keys: ["f/" + octaaf], duration: "q"}) ];
                        break;
                    case "Fa#":
                        noteArray = [ new Vex.Flow.StaveNote({ keys: ["f#/" + octaaf], duration: "q"}).addAccidental(0, new Vex.Flow.Accidental("#")) ];
                        break;
                    case "Sol":
                        noteArray = [ new Vex.Flow.StaveNote({ keys: ["g/" + octaaf], duration: "q"}) ];
                        break;
                    case "Sol#":
                        noteArray = [ new Vex.Flow.StaveNote({ keys: ["g#/" + octaaf], duration: "q"}).addAccidental(0, new Vex.Flow.Accidental("#")) ];
                        break;
                    case "La":
                        noteArray = [ new Vex.Flow.StaveNote({ keys: ["a/" + octaaf], duration: "q"}) ];
                        break;
                    case "Sib":
                        noteArray = [ new Vex.Flow.StaveNote({ keys: ["bb/" + octaaf], duration: "q"}).addAccidental(0, new Vex.Flow.Accidental("b")) ];
                        break;
                    case "Si":
                        noteArray = [ new Vex.Flow.StaveNote({ keys: ["b/" + octaaf], duration: "q"}) ];
                        break;
                    default :
                        noteArray = [ new Vex.Flow.StaveNote({ keys: ["g/" + octaaf], duration: "q"}) ];
                        break;
                }
                
            return noteArray;
        }
        //pursuit functions to check if the noteToPlay is correct
        function checkIfIsNote(noteToPlay, position, score) {
                var isNotNote = true;
                //als we op een tijdsnotering of dergelijke komen, willen we deze overslaan.
                //dit kunnen er meerderer zijn, vandaar de while
                if(noteToPlay.substring(noteToPlay.length -2, noteToPlay.length - 1) == "/" || noteToPlay == ""){
                    isNotNote = false;
                }
                while(isNotNote == true){
                //check if the note is an note and adapt if neccissary 
                var data = isNoteBar(noteToPlay, position, score);
                data = isNoteTiming(data[0], data[1], score);
                data = hasNoteSymbol(data[0], data[1], score);
                noteToPlay = data[0];
                position = data[1];
                //is the note a note now or do we have to do the while again?
                    if(noteToPlay.substring(noteToPlay.length - 2, noteToPlay.length - 1) == "/"){
                        isNotNote = false;
                    }
                }
                return [noteToPlay, position];
            }
        function isNoteBar(noteToPlay, position, score){
                    if(noteToPlay.indexOf('|') > -1){//de noteToPlay mag geen maatstreep zijn
                        position = position + 2;
                        noteToPlay = score.substring(position, position + 3);
                    }
                    return [noteToPlay, position];
                }
                function isNoteTiming(noteToPlay, position, score){
                    if(noteToPlay.indexOf(':8') > -1 || //mag geen achtste zijn
                        noteToPlay.indexOf(':q') > -1 || //mag geen kwart zijn
                        noteToPlay.indexOf(':h') > -1){ //mag geen hele zijn

                            if( noteToPlay.substring(2, 3) == "d"){  //gepunteerden zijn een karakter langer   
                                position = position + 4;
                            }
                            
                            else if(score.substring(position + 1, position +2) == "8" ||
                                    score.substring(position + 1, position +2) == "q" ||
                                    score.substring(position + 1, position +2) == "h"){
                                position = position + 3;
                            }
                            
                            noteToPlay = score.substring(position, position + 3);
                    }
                    if(noteToPlay.indexOf(':1') > -1){
                        position = position + 4;
                        noteToPlay = score.substring(position, position + 3);
                    }
                    return [noteToPlay, position];
                }
                function hasNoteSymbol(noteToPlay, position, score){
                    if(score.substring(position + 1, position +2) == "#" ||
                        score.substring(position + 1, position +2) == "n" ||
                        score.substring(position + 1, position +2) == "@"){
                            noteToPlay = score.substring(position, position + 4);
                            position++;
                    }
                    return [noteToPlay, position];
                }

        //toevalligheden
        function autoCorrelate( buf, sampleRate ) {
            var SIZE = buf.length;
            var MAX_SAMPLES = Math.floor(SIZE/2);
            var best_offset = -1;
            var best_correlation = 0;
            var rms = 0;
            var foundGoodCorrelation = false;
            var correlations = new Array(MAX_SAMPLES);
            var MIN_SAMPLES = 0;  // will be initialized when AudioContext is created.

            for (var i=0;i<SIZE;i++) {
                var val = buf[i];
                rms += val*val;
            }
            rms = Math.sqrt(rms/SIZE);
            if (rms<0.01) // not enough signal
                return -1;

            var lastCorrelation=1;
            for (var offset = MIN_SAMPLES; offset < MAX_SAMPLES; offset++) {
                var correlation = 0;

                for (var i=0; i<MAX_SAMPLES; i++) {
                    correlation += Math.abs((buf[i])-(buf[i+offset]));
                }
                correlation = 1 - (correlation/MAX_SAMPLES);
                correlations[offset] = correlation; // store it, for the tweaking we need to do below.
                if ((correlation>0.9) && (correlation > lastCorrelation)) {
                    foundGoodCorrelation = true;
                    if (correlation > best_correlation) {
                        best_correlation = correlation;
                        best_offset = offset;
                    }
                } else if (foundGoodCorrelation) {
                    // short-circuit - we found a good correlation, then a bad one, so we'd just be seeing copies from here.
                    // Now we need to tweak the offset - by interpolating between the values to the left and right of the
                    // best offset, and shifting it a bit.  This is complex, and HACKY in this code (happy to take PRs!) -
                    // we need to do a curve fit on correlations[] around best_offset in order to better determine precise
                    // (anti-aliased) offset.

                    // we know best_offset >=1,
                    // since foundGoodCorrelation cannot go to true until the second pass (offset=1), and
                    // we can't drop into this clause until the following pass (else if).
                    var shift = (correlations[best_offset+1] - correlations[best_offset-1])/correlations[best_offset];
                    return sampleRate/(best_offset+(8*shift));
                }
                lastCorrelation = correlation;
            }
            if (best_correlation > 0.01) {
                // console.log("f = " + sampleRate/best_offset + "Hz (rms: " + rms + " confidence: " + best_correlation + ")")
                return sampleRate/best_offset;
            }
            return -1;
        //	var best_frequency = sampleRate/best_offset;
        }
        
        

        function nth_occurrence (string, char, nth) {
            var first_index = string.indexOf(char);
            var length_up_to_first_index = first_index + 1;

            if (nth == 1) {
                return first_index;
            } else {
                var string_after_first_occurrence = string.slice(length_up_to_first_index);
                var next_occurrence = nth_occurrence(string_after_first_occurrence, char, nth - 1);

                if (next_occurrence === -1) {
                    return -1;
                } else {
                    return length_up_to_first_index + next_occurrence;  
                }
            }
        }
        
        function setOctaveToInstrument(){
            var instrument = $('.instrument li.active').text();
            console.log($('.instrument li.active').text());
            console.log('octaaf eerst : ' + octaaf);
            switch(instrument){
                case "Sopranino":
                    break;
                case "Sopraan":
                    break;
                case "Alt":
                    octaaf = octaaf + 1
                    break;
                case "Tenor":
                    octaaf = octaaf + 1
                    break;
            }
            console.log(octaaf)
        }
        
        }
    
})();
;(function() {
    'use strict';
    
    angular.module('app.studies')
    
    .factory('Studies', Studies)
    .factory('RankedStudies', RankedStudies);
    
    //Studies.$inject['$resource'];
    
    function Studies($resource) {
        return $resource('api/studies/:studyId', {
            studyId: '@_id'
        });
    }
    
    function RankedStudies($resource) {
        return $resource('api/studies-rank/:rank', {
            rank: '@rank'
        });
    }
    
})();

angular.module('todos').config(['$routeProvider',
    function($routeProvider) {
        $routeProvider.
        when('/todos', {
            templateUrl: 'todos/views/list-todos.client.view.html',
            controller: 'TodosController'
        }).
        when('/todos/create', {
            templateUrl: 'todos/views/create-todo.client.view.html',
            controller: 'TodosController'
        }).
        when('/todos/:todoId', {
            templateUrl: 'todos/views/view-todo.client.view.html',
            controller: 'TodosController'
        }).
        when('/todos/:todoId/edit', {
            templateUrl: 'todos/views/edit-todo.client.view.html',
            controller: 'TodosController'
        });
    }
]);

angular.module('todos').controller('TodosController', ['$scope', '$routeParams', '$location', 'Authentication', 'Todos',
    function($scope, $routeParams, $location, Authentication, Todos) {
        $scope.authentication = Authentication;

        $scope.create = function() {
            var todo = new Todos({
                title: this.title,
                comment: this.comment
            });

            todo.$save(function(response) {
                $location.path('todos/' + response._id);
            }, function(errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };

        $scope.find = function() {
            $scope.todos = Todos.query();
        };

        $scope.findOne = function() {
            $scope.todo = Todos.get({
                todoId: $routeParams.todoId
            });
        };

        $scope.update = function() {
            $scope.todo.$update(function() {
                $location.path('todos/' + $scope.todo._id);
            }, function(errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };

        $scope.delete = function(todo) {
            if (todo) {
                todo.$remove(function() {
                    for (var i in $scope.todos) {
                        if ($scope.todos[i] === todo) {
                            $scope.todos.splice(i, 1);
                        }
                    }
                });
            } else {
                $scope.todo.$remove(function() {
                    $location.path('todos');
                });
            }
        };
    }
]);

angular.module('todos').factory('Todos', ['$resource',
    function($resource) {
        return $resource('api/todos/:todoId', {
            todoId: '@_id'
        }, {
            update: {
                method: 'PUT'
            }
        });
    }
]);

;(function() {
    'use strict';
    angular.module('app.tests')
    .config(Routes);
    
    function Routes($routeProvider) {
        
        $routeProvider.
        when('/test', {
            templateUrl: 'templates/tests/views/test.client.view.html',
            controller: 'TestController'
        });
        
    }
})();


;(function() {
    'use strict';
    
    angular.module('app.tests')
    
    .controller('TestController', TestController);
    
    //PickAStudyController.$inject['$scope', '$routeParams', '$location', 'Authentication'];
     
    function TestController($timeout, $scope, $rootScope, $location, ChatUsers, ngDialog, TestRecordings, Recordings, Audio, Pubnub, Realtime, VidChat, Gamification, TestMoments) {
        var head= document.getElementsByTagName('head')[0];
        var script= document.createElement('script');
        
        //laadt het menu
        $scope.user = $rootScope.user
        $scope.menuTemplate = "templates/partials/menu.html";
        
        $scope.status = "notLoggedIn";
        
        Realtime.init();
        
        //REALTIME CHECKLIST
        //De checklist die dient om te quoteren als er een toets afgenomen wordt
        $scope.checklist = {};
        
        var checkChannel = 'checklist-channel';
        Realtime.subscribeToChannel(checkChannel);
        $scope.watchChecklist = function(){
            var list = $scope.checklist;
            var count = 0;
            for (var i in list) {
                if (list.hasOwnProperty(i)) {
                ++count;
                }
            }
            if(count >= 4){
                $scope.checklist.result = list.result = list.tempo + list.rythm + list.notes + list.musicality;
            }
            Realtime.sendMessage(list, checkChannel);
        }
        $scope.$on(Pubnub.getMessageEventNameFor(checkChannel), function (ngEvent, m) {
            $scope.$apply(function () {
                $scope.checklist = m;
            });
        });
       
       //REALTIME ONLINE KOMEN
       //Gebruikers die on/offline gaan voor een toets wordt realtime weergegeven
        var channel = 'chatUser-channel';
        Realtime.subscribeToChannel(channel);
        var object = {
                        username: $scope.user.username,
                        delete: false
                    };
        $scope.sendMessage = function(){
            Realtime.sendMessage(object, channel);
        }
        
        $scope.$on(Pubnub.getMessageEventNameFor(channel), function (ngEvent, m) {
            if(m.delete == false){
                delete m.delete; //we willen de delete-prop niet bij de chatUsers pushen
                var exists = false; 
                $scope.chatUsers.forEach(function(chatUser) {
                    if(chatUser.username == m.username){
                        exists = true;
                    }
                });
                if(exists == false){
                    $scope.$apply(function () {
                        $scope.chatUsers.push(m)
                    });
                }
            }
            else{
                var index = 0;
                $scope.chatUsers.forEach(function(chatUser) {
                    if(chatUser.username == m.username){
                        $scope.chatUsers.splice(index, 1);
                        $scope.$apply(function () {
                            $scope.chatUsers;
                        });
                    }
                    index++;
                });
            }
        });
       
       //CRUD
        $scope.find = function() {
            $scope.chatUsers = ChatUsers.query(function(data){
                $scope.chatUsers = data;
            });
        }
        $scope.getRecordings = function(){
            console.log($scope.user._id);
            $scope.recordings = TestRecordings.query({
                userId: $scope.user._id
            }).$promise.then(function(data){
                console.log(data);
                $timeout(function() {
                    console.log(data);
                    $scope.recordings = data;
                });
            });
        }
        $scope.getTestMoments = function(){
            $scope.testMoments = TestMoments.query(function(data){
                $scope.testMoments = data;
            });
        }
        $scope.addTestMoment = function(){
            var testmoment = new TestMoments({
                username: $scope.user.username, 
                date: new Date($scope.testMoment.date),
                startHour: new Date($scope.testMoment.start),
                endHour: new Date($scope.testMoment.end),
                personsAllowed: $scope.testMoment.places
            });
            testmoment.$save(function(response) {
                console.log(response);
            }, function(errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        }
        

        
        
        
        //VIDEOCHAT
        //Je moet een liedje gekozen hebben voor je kan inloggen als je geen gouden rank bent
        $scope.checkLogin = function() {
            var recording = $('#recordings input[type=radio]:checked').val();
            if(recording !== undefined || $scope.user.rank.title == 'Gouden rank'){
                login();
            }else{
                alert("Selecteer eerst het stuk dat je wilt spelen"); }
        }
        
        var login = function() {
            $scope.status = "loggingIn";
            
            createChatUser();
            
            $scope.phone = VidChat.createPhone($scope.user.username);
            $scope.phone.ready(function(){ 
                $scope.status = "loggedIn";
                $scope.$apply();
            });
            $scope.phone.receive(function(session){
                session.connected(function(session) {
                    var username;
                    //de gouden-rank gebruiker
                    if($scope.chatUsername !== undefined){
                        username = $scope.chatUsername;
                    }else{//diegene die de toets doet
                        username = $scope.chatUser.username;
                    }
                    $scope.chatData = ChatUsers.query({
                        userName: username
                    }).$promise.then(function(chatData){
                        $scope.chatData = chatData[0];
                        openDialog();
                        fillDialog(session);
                        })
                });
                session.ended(function() { $scope.phone.hangup(); });
            });
        }
        
        $scope.makeCall = function (username){
            $scope.chatUsername = VidChat.call(username);}
        
        var openDialog = function(){
            ngDialog.openConfirm({
                template: 'templates/partials/testChat.html',
                className: 'ngdialog-theme-default',
                scope: $scope, // <- ability to use the scopes from directive controller
            }).then(function (success) {
                Gamification.checkForRankUp($scope.chatUsername, $scope.checklist.result);
            }, function (error) {
               
            });
        }
        
        var fillDialog = function(session){
            $rootScope.$on('ngDialog.opened', function (e, $dialog) {
                document.getElementById('incomingVid').appendChild(session.video);
                var thumbDiv = document.getElementById('outgoingVid');
                $scope.phone.addLocalStream(thumbDiv);
                
                Audio.placeTag('audio', $scope.chatData.songData);
            }); 
        }
        
        
        var createChatUser = function() {
            if($scope.user.rank.title !== 'Gouden rank'){
                Recordings.get({
                    recordingId: $('#recordings input[type=radio]:checked').val()
                }).$promise.then(function(recording){
                    fillUserAndScope(recording);
                });
            }
        };
        
        function fillUserAndScope(recording){
            var user = $scope.user;
            VidChat.fillChatUserAndSave(recording, user).then(function(chatUser){
                $scope.chatUser = chatUser;
                $scope.chatUsers.push(chatUser);
                Realtime.sendMessage(object, channel); 
            });
        }
        
        $scope.$on('$locationChangeSuccess', function(){
             object.delete = true;
             Realtime.sendMessage(object, channel); 
             VidChat.logout($scope.user.rank.title, $scope.status, $scope.user.username);
        });
        
        window.addEventListener("beforeunload", function (e) {
            if($location.path() == "/test"){
                object.delete = true;
                Realtime.sendMessage(object, channel); 
                VidChat.logout($scope.user.rank.title, $scope.status, $scope.user.username);
                 
            }
        });
    }
})();

;(function() {
    'use strict';
    
    angular.module('app.tests')
    
    .factory('ChatUsers', ChatUsers)
    .factory('TestRecordings', TestRecordings)
    .factory('TestMoments', TestMoments);
    
    //Studies.$inject['$resource'];
    
    function ChatUsers($resource) {
        return $resource('api/chatUsers/:chatUserName', {
            chatUserName: '@username'
        }, {
            'query':  {method:'GET', isArray:true}
        });
    }
    
    function TestRecordings($resource) {
        return $resource('api/testRecordings/:userId', {
            recordingId: '@userId'
        }, {
            'query':  {method:'GET', isArray:true}
        });
    }
    
    function TestMoments($resource){
        return $resource('api/testmoments/', 
         {
            'save':  {method:'POST'}
        });
    }
    
})();

;(function() {
    'use strict';
    angular.module('app.tests')
    .factory('Realtime', Realtime);
    
    function Realtime(Pubnub){
       
        var uuid = Math.floor((Math.random() * 100) + 1); 
       
        var service = {
            init: init,
            subscribeToChannel: subscribeToChannel,
            sendMessage: sendMessage
        };
        
        return service;
       
        function init(){
            Pubnub.init({
                publish_key   : 'pub-c-27dffc2d-ccd3-4674-8413-c4cdcd8703ad',
                subscribe_key : 'sub-c-74d07376-fb25-11e5-8679-02ee2ddab7fe',
                uuid: uuid
            }); 
        }
        
        function subscribeToChannel(channel){
            Pubnub.subscribe({
                channel: channel,
                triggerEvents: ['callback']
            });
        }
        
        function sendMessage(object, channel){
            Pubnub.publish({
                channel: channel,
                message: object,
                callback: function(m) {
                    console.log(m);
                }
            });
        }
    }
    
})();
;(function() {
    'use strict';
    angular.module('app.tests')
    .factory('VidChat', VidChat);
    
    function VidChat(ChatUsers, $q){
       
       
        var service = {
            createPhone: createPhone,
            call: call,
            fillChatUserAndSave: fillChatUserAndSave,
            logout: logout
        };
        
        return service;
       
       function createPhone(username){
            var phone = window.phone = PHONE({
                number        : username || "Anonymous", // listen on username line else Anonymous
                publish_key   : 'pub-c-27dffc2d-ccd3-4674-8413-c4cdcd8703ad',
                subscribe_key : 'sub-c-74d07376-fb25-11e5-8679-02ee2ddab7fe',
            });	
            console.log('phone created');
             var ctrl = window.ctrl = CONTROLLER(phone);
            return ctrl;
        } 
        
        function logout(rankTitle, status, username){
            if(rankTitle !== 'Gouden rank' && status == "loggedIn"){
                ChatUsers.get({
                    chatUserName: username
                }).$promise.then(function(chatUser){
                    console.log(chatUser);
                    chatUser.$remove(function(chatUser){
                    
                    },function(errorResponse) {
                        console.log(errorResponse);
                    });
                });
            }
        }
        
        function call(username){
            if (!window.phone) alert("Login First!");
            else phone.dial(username);
            return username;
        }
        
        function fillChatUserAndSave(recording, user){
            return $q( function(resolve, reject) {
                
                var chatUser = new ChatUsers({
                    username: user.username,
                    currentRank: user.rank.title,
                    songTitle: recording.title,
                    songAuthor: recording.author,
                    songData: recording.data,
                    songChecklist: recording.testChecklist
                });
                        
                chatUser.$save(function(response) {
                    resolve(chatUser);
                }, function(errorResponse) {
                    reject(errorResponse.data.message);
                });
                
            });
        }
        
    }
    
})();
;(function() {
    'use strict';
    angular.module('app.users')
    .config(Routes);
    
    //Routes.$inject['$routeProvider'];
    
    function Routes($routeProvider) {
        
        $routeProvider.
        when('/', {
            templateUrl: 'templates/useri/views/home.client.view.html',
            controller: 'UsersController'
        }).
        when('/profile/:profileId', {
            templateUrl: 'templates/useri/views/edit-user.client.view.html',
            controller: 'UsersController'
        });
        
    }
})();
;(function() {
    'use strict';
    
    angular.module('app.users')
    
    .controller('UsersController', UsersController);
    
    //UsersController.$inject['$scope', '$routeParams', '$location', 'Authentication', 'Users', '$rootScope', 'NoteService'];
    
    function UsersController($scope, $routeParams, $location, Authentication, Users, $rootScope, NoteService) {
        //de aangemelde gebruiker
        if(!$rootScope.user){
            //dit is de gebruiker die we krijgen bij het aanmelden
            //wanneer we deze zouden updaten is deze niet meer relevant
            $scope.user = $rootScope.user = Authentication.user;
        }else{
            $scope.user = $rootScope.user;
            $scope.earnedPoints = $rootScope.earnedPoints;
            $scope.learnedNote = $rootScope.learnedNote;
        }
         $rootScope.user = $scope.user;
        //laadt het menu
        $scope.menuTemplate = "templates/partials/menu.html";     

        $scope.update = function() {
            $scope.user.$update(function() {
                $rootScope.user = $scope.user;
                $location.path('/');
            }, function(errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };
    }
})();

;(function() {
    'use strict';
    angular.module('app.users')
    .factory('Users', Users)
    .factory('Authentication', Authentication)
    .factory('UserByName', UserByName);
    
    //Users.$inject['$resource'];
    
    function Authentication() {
        this.user = window.user;
        
        return{
            user: this.user  
        }; 
    }
    
    function Users($resource) {
        return $resource('api/users/:userId', {
            userId: '@_id'
        }, {
            update: {
                method: 'PUT'
            }
        });
    }
    
    function UserByName($resource) {
        return $resource('/api/usersByName/:userName', {
            userName: '@username'
        },
        {
            query: {
                method:'GET', isArray:true
            }, 
            update: {
                method: 'PUT'
            }});
    }
   
})();
;(function() {
    'use strict';
    angular.module('app.users')
    .factory('Levels', Levels)
    .factory('Ranks', Ranks)
    .factory('Gamification', Gamification);
    
    function Levels($resource) {
        return $resource('api/levels/:name', {
            name: '@name'
        });
    }
    function Ranks($resource){
        return $resource('/api/ranks/:rankValue', {
            rankValue: '@value'
        },{
            query: {
                method:'GET', isArray:true
            } });
    }
    
    function Gamification(Levels, $q, $rootScope, UserByName, Ranks){
        
        var service = {
            checkForLevelUp: checkForLevelUp,
            checkForRankUp: checkForRankUp
        };
        
        return service;
        
        function checkForLevelUp(user, earnedpoints){
            $rootScope.earnedPoints = earnedpoints;
            
            return $q( function(resolve, reject) {
                
                var totalpoints = user.profile.points + earnedpoints;
                
                if((totalpoints) >= user.level.points){
                                        
                    var leftOvers = totalpoints - user.level.points;
                    
                    Levels.query({
                        name: user.level.name + 1
                    }).$promise.then(function(response){                        
                        //fill in new data                            
                        user.profile.points = leftOvers;
                        user.level.name = response[0].name;
                        user.level.points = response[0].points;            

                        resolve(user);      
                    }).catch(function(err){console.log(err)})
                    
                }else{
                    user.profile.points = totalpoints;
                    resolve(user); 
                }
            })
        }
        
        function checkForRankUp(username, earnedpoints){
            
            return $q( function(resolve, reject) {
                if(earnedpoints >= 50){
                    UserByName.get({
                        userName: username
                    }).$promise.then(function(user){
                        console.log(user);                        
                        var nextRank = user.rank.value + 1;          
                        Ranks.query({
                            rankValue: nextRank
                        }).$promise.then(function(rank){
                            user.rank = rank[0];
                            console.log('user: ');
                            console.log( user);
                            console.log('newRank: ');
                            console.log( rank);
                            user.$update(function(response){
                                console.log('success: ' );
                                console.log( response);
                            }, function(errorResponse) {
                                console.log(errorResponse);
                            });
                        })
                        resolve(user);      
                    }).catch(function(err){console.log(err)})
                }
                else{
                    resolve('Te lage score');
                }
            })
        }
    }
    
})();
;(function() {
    'use strict';
    angular.module('app.users')
    .factory('Notes', Notes)
    .service('NoteService', NoteService);
    
    //Notes.$inject['$resource'];
    
    function Notes($resource) {
        return $resource('api/notes/:noteId', {
            userId: '@_id'
        });
    }
    
    function NoteService(Notes, $q){
        var service = {
            CheckIfNoteLearned: CheckIfNoteLearned,
            CountLearnedNotes: CountLearnedNotes
        };
        
        return service; 
            
        function CheckIfNoteLearned(note, user){
            if(user.learnedNotes.indexOf(note._id) !== -1){
                return true;
            }else{
                return false;
            }
        }            
        function CountLearnedNotes(treble, user){
            return $q( function(resolve, reject) {
                Notes.query().$promise.then(function(notes){
                    var counter = 0;
                    
                    notes.forEach(function(note){
                        if(note.treble == treble){
                            if(CheckIfNoteLearned(note, user)){
                               counter++;
                            }
                        }
                    })
                    resolve(counter);
                })
            })
        }   
    }
    
        
})();
;(function() {
    'use strict';
    angular.module('app_backoffice.home')
    .config(Routes);
    
    //Routes.$inject['$routeProvider'];
    
    function Routes($routeProvider) {
        
        $routeProvider.
        when('/', {
            templateUrl: 'templates/backend/home/views/home.client.view.html',
            controller: 'HomeController'
        });
        
    }
})();
;(function() {
    'use strict';
    angular.module('app_backoffice.home')
    .factory('Users', Users)
    .factory('Authentication', Authentication);
    
    //Users.$inject['$resource'];
    
    function Authentication() {
        this.user = window.user;
        
        return{
            user: this.user  
        }; 
    }
    
    function Users($resource) {
        return $resource('api/users/:userId', {
            userId: '@_id'
        }, {
            update: {
                method: 'PUT'
            }
        });
    }
    

   
})();
;(function() {
    'use strict';
    
    angular.module('app_backoffice.home')
    
    .controller('HomeController', HomeController);
    
    //UsersController.$inject['$scope', '$routeParams', '$location', 'Authentication', 'Users', '$rootScope', 'NoteService'];
    
    function HomeController($scope, Authentication) {
        $scope.menuTemplate = "templates/backend/partials/menu.html";
 
    }
})();

;(function() {
    'use strict';
    angular.module('app_backoffice.recordings')
    .config(Routes);
    
    //Routes.$inject['$routeProvider'];
    
    function Routes($routeProvider) {
        
        $routeProvider.
        when('/recordings', {
            templateUrl: 'templates/backend/recordings/views/recordings.client.view.html',
            controller: 'RecordingsController'
        })
        .when('/recordings/:recordingId/edit', {
            templateUrl: 'templates/backend/recordings/views/recording-edit.client.view.html',
            controller: 'RecordingsController'
        })
        .when('/recordings/create', {
            templateUrl: 'templates/backend/recordings/views/recording-create.client.view.html',
            controller: 'RecordingsController'
        })
        .when('/recordings/:recordingId', {
            templateUrl: 'templates/backend/recordings/views/recording-details.client.view.html',
            controller: 'RecordingsController'
        });
        
    }
})();
;(function() {
    'use strict';
    
    angular.module('app_backoffice.recordings')
    
    .controller('RecordingsController', RecordingsController);
    
    //RecordingsController.$inject['$scope', '$routeParams', '$location', 'Authentication', 'Recordings', '$rootScope', 'NoteService'];
    
    function RecordingsController($scope, Recordings, $routeParams, $location, $sce) {
        $scope.menuTemplate = "templates/backend/partials/menu.html";
                
        $scope.create = function() {
            
            var instruments = {}
        
            if(this.instruments.sopranino == true)
                instruments.sopranino = { "name" : "Sopranino"}
            
            var recording = new Recordings({                
                name : this.name,
                author : this.author,
                description : this.description,
                rank : this.rank,
                genre : this.genre,
                focus : this.focus,
                time:  this.time,
                key : this.key,
                notes : this.notes,
                perfectDuration : this.perfectDuration, 
                maxPoints : this.maxPoints, 
                instruments,
                checklist : {
                    tempo : this.checklist.tempo,
                    notes : this.checklist.notes
                }
            });

            recording.$save(function(response) {
                $location.path('/recordings');
            }, function(errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };

        $scope.find = function() {
            $scope.recordings = Recordings.query();
        };

        $scope.findOne = function() {
            $scope.recording = Recordings.get({
                recordingId: $routeParams.recordingId
            }).$promise.then(function(audio){
                var audioElem = document.createElement("audio");
                audioElem.setAttribute("controls", "controls");
                var src = document.createElement("source");
                src.setAttribute('src', trustAudio(audio.data));
                src.setAttribute('type', 'audio/ogg');
                audioElem.appendChild(src);
                var element = document.getElementById('audio');
                element.appendChild(audioElem); 
            
                $scope.recording = audio;
                function trustAudio(audio) {
                    return $sce.trustAsResourceUrl(audio);
                }
            });
            
            
        };

        $scope.update = function() {
            $scope.recording.$update(function() {
                $location.path('recordings');
            }, function(errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };

        $scope.delete = function(recording) {
            if (recording) {
                $scope.recording = Recordings.get({
                    recordingId: recording
                }).$promise.then(function(recording){
                    recording.$remove(function() {
                        for (var i in $scope.recordings) {
                            if ($scope.recordings[i] === recording) {
                                $scope.$apply(function(){
                                    $scope.recordings.splice(i, 1);
                                });
                            }
                        }
                    });
                    $location.path('/recordings');
                });
            } 
        };
        
        $scope.deleteFeedback = function(recording_id, feedback){
            if (recording_id) {
                $scope.recording = Recordings.get({
                    recordingId: recording_id
                }).$promise.then(function(recording){
                   
                    for (var i in recording.feedbacks) {
                         console.log(recording.feedbacks[i]);
                         console.log(feedback);
                        if (recording.feedbacks[i].body == feedback.body
                             && recording.feedbacks[i].username == feedback.username) {
                            recording.feedbacks.splice(i, 1);
                            console.log('sliced');
                        }
                    }
                    $scope.$apply(function(){
                        $scope.recording.feedbacks = recording.feedbacks;
                        });
                    recording.$update(function() {
                        $location.path('recordings/' + recording_id);
                    }, function(errorResponse) {
                        $scope.error = errorResponse.data.message;
                    });
                });
            } 
        }
    }
})();

;(function() {
    'use strict';
    angular.module('app_backoffice.recordings')
    .factory('Recordings', Recordings);
    
    //Recordings.$inject['$resource'];
       
    function Recordings($resource) {
        return $resource('api/recordings/:recordingId', {
            recordingId: '@_id'
        }, {
            update: {
                method: 'PUT', 
            }
        });
    }
    

   
})();
;(function() {
    'use strict';
    angular.module('app_backoffice.studies')
    .config(Routes);
    
    //Routes.$inject['$routeProvider'];
    
    function Routes($routeProvider) {
        
        $routeProvider.
        when('/studies', {
            templateUrl: 'templates/backend/studies/views/studies.client.view.html',
            controller: 'StudiesController'
        })
        .when('/studies/:studyId/edit', {
            templateUrl: 'templates/backend/studies/views/study-edit.client.view.html',
            controller: 'StudiesController'
        })
        .when('/studies/create', {
            templateUrl: 'templates/backend/studies/views/study-create.client.view.html',
            controller: 'StudiesController'
        })
        .when('/studies/:studyId', {
            templateUrl: 'templates/backend/studies/views/study-details.client.view.html',
            controller: 'StudiesController'
        });
        
    }
})();
;(function() {
    'use strict';
    
    angular.module('app_backoffice.studies')
    
    .controller('StudiesController', StudiesController);
    
    //StudiesController.$inject['$scope', '$routeParams', '$location', 'Authentication', 'Studies', '$rootScope', 'NoteService'];
    
    function StudiesController($scope, Studies, $routeParams, $location) {
        $scope.menuTemplate = "templates/backend/partials/menu.html";
        
        $scope.create = function() {
            
            var instruments = {}
        
            if(this.instruments.sopranino == true)
                instruments.sopranino = { "name" : "Sopranino"}
            
            var study = new Studies({                
                name : this.name,
                author : this.author,
                description : this.description,
                rank : this.rank,
                genre : this.genre,
                focus : this.focus,
                time:  this.time,
                key : this.key,
                notes : this.notes,
                perfectDuration : this.perfectDuration, 
                maxPoints : this.maxPoints, 
                instruments,
                checklist : {
                    tempo : this.checklist.tempo,
                    notes : this.checklist.notes
                }
            });

            study.$save(function(response) {
                $location.path('/studies');
            }, function(errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };

        $scope.find = function() {
            $scope.studies = Studies.query();
        };

        $scope.findOne = function() {
            $scope.study = Studies.get({
                studyId: $routeParams.studyId
            });
        };

        $scope.update = function() {
            $scope.study.$update(function() {
                $location.path('studies');
            }, function(errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };

        $scope.delete = function(study) {
            if (study) {
                $scope.study = Studies.get({
                    studyId: study
                }).$promise.then(function(study){
                    study.$remove(function() {
                        for (var i in $scope.studies) {
                            if ($scope.studies[i] === study) {
                                $scope.studies.splice(i, 1);
                            }
                        }
                    });
                    $location.path('/studies');
                });
                
            } 
        };
    }
})();

;(function() {
    'use strict';
    angular.module('app_backoffice.studies')
    .factory('Studies', Studies);
    
    //Studies.$inject['$resource'];
       
    function Studies($resource) {
        return $resource('api/studies/:studyId', {
            studyId: '@_id'
        }, {
            update: {
                method: 'PUT', 
            }
        });
    }
    

   
})();
;(function() {
    'use strict';
    
    angular.module('app_backoffice.users')
    
    .controller('UsersController', UsersController);
    
    //UsersController.$inject['$scope', '$routeParams', '$location', 'Authentication', 'Users', '$rootScope', 'NoteService'];
    
    function UsersController($scope, Users, $routeParams, $location) {
        $scope.menuTemplate = "templates/backend/partials/menu.html";
        
        $('.datepicker').datepicker({yearRange: "-80:+0", changeYear: true, dateFormat: 'dd-mm-yy' });
        
        $scope.create = function() {
            var user = new Users({
                username: this.username,
                password: this.password,
                profile: {
                    given_name: this.profile.given_name,
                    family_name: this.profile.family_name,
                    email: this.profile.email,
                    birthday: $("#birth-backoffice").datepicker('getDate')                   
                }
            });

            user.$save(function(response) {
                $location.path('/users');
            }, function(errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };

        $scope.find = function() {
            $scope.users = Users.query();
        };
        
        $scope.findOne = function() {
            $scope.user = Users.get({
                userId: $routeParams.userId
            });
        };

        $scope.update = function() {
            $scope.user.$update(function() {
                $location.path('users');
            }, function(errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };

        $scope.delete = function(user) {
            if (user) {
                $scope.user = Users.get({
                    userId: user
                }).$promise.then(function(user){
                    user.$remove(function() {
                        for (var i in $scope.users) {
                            if ($scope.users[i] === user) {
                                $scope.$apply(function(){
                                    $scope.users.splice(i, 1);
                                });
                            }
                        }
                    });
                    $location.path('/users');
                });
                
            } 
        };
    }
})();

;(function() {
    'use strict';
    angular.module('app_backoffice.users')
    .config(Routes);
    
    //Routes.$inject['$routeProvider'];
    
    function Routes($routeProvider) {
        
        $routeProvider.
        when('/users', {
            templateUrl: 'templates/backend/useri/views/users.client.view.html',
            controller: 'UsersController'
        })
        .when('/users/:userId/edit', {
            templateUrl: 'templates/backend/useri/views/user-edit.client.view.html',
            controller: 'UsersController'
        })
        .when('/users/create', {
            templateUrl: 'templates/backend/useri/views/user-create.client.view.html',
            controller: 'UsersController'
        })
        .when('/users/:userId', {
            templateUrl: 'templates/backend/useri/views/user-details.client.view.html',
            controller: 'UsersController'
        });
        
    }
})();
;(function() {
    'use strict';
    angular.module('app_backoffice.users')
    .factory('Users', Users);
    
    //Users.$inject['$resource'];
       
    function Users($resource) {
        return $resource('api/users/:userId', {
            userId: '@_id'
        }, {
            update: {
                method: 'PUT', 
            }
        });
    }
    

   
})();