# Recorder

Deze site laat gebruikers toe om blokfluit te leren spelen. De site wordt ontwikkeld met behulp van de MEAN-stack en dient als bachelorproef voor mijn opleiding grafische en digitale media.

## Installation

1. Download het project:   ```git clone https://ellenvervaet@bitbucket.org/ellenvervaet/recorder.git```
2. Installeer de node packages: ```npm install```
3. Vergeet ook bower niet: ```bower install```

## Deployment

1. Seed de database: ```node seeder```
2. Start mongoDB op (en [installeer](https://www.mongodb.com/download-center#community) indien nodig):  ```mongod```. (Eventueel definieer je -dbpath in dit commando bv: ```mongod -dbpath "C:\Program Files\MongoDB\Server\3.2\data\db"```).
3. Start de server: ```node server```

## Domeinen en inloggegevens

### Frontoffice

[jasmyn.be](jasmyn.be)


**Inloggegevens gewone gebruiker**

Gebruikersnaam en wachtwoord: recorder-user

**Inloggegevens hogere rank gebruiker**

Gebruikersnaam en wachtwoord: recorder-admin

### Backoffice

[jasmyn.be/backoffice](jasmyn.be/backoffice)

**Inloggegevens admin**

Gebruikersnaam en wachtwoord: recorder-admin