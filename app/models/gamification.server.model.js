var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var BadgeSchema = new Schema({
    title: String,
    description: String,
    image: String,
    points: Number,
});
mongoose.model('Badge', BadgeSchema);

var LevelSchema = new Schema({
    name: Number,
    points: Number
});
mongoose.model('Level', LevelSchema);

var RankSchema = new Schema({
    title: String,
    value: Number,
    description: String,
    image: String,
});
mongoose.model('Rank', RankSchema);