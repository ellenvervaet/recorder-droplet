var mongoose = require('mongoose'),
    crypto = require('crypto'), 
    Schema = mongoose.Schema;
    
var ChatUserSchema = new Schema({
        username: String,
        currentRank: String,
        songTitle: String,
        songAuthor: String,
        songData: String,
        songChecklist: { }
});

mongoose.model('ChatUser', ChatUserSchema);