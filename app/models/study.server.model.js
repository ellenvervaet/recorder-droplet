var mongoose = require('mongoose'),
    crypto = require('crypto'), 
    Schema = mongoose.Schema;
    
var StudySchema = new Schema({
    
        genre: String,
        testChecklist: {
            tempo : Number,
            notes : Number,
            rythm : Number,
            musicality : Number
        },
        name: String,
        author: String,
        description: String,
        difficulty: Number,
        rank: Number,
        focus: String,
        tempo: Number,
        time: String,
        key: String,
        notes: String,
        perfectDuration: Number,
        instruments: {
        },
        maxPoints: Number,
        checklist: {
            tempo: Number,
            notes: Number,
        }
});

mongoose.model('Study', StudySchema);