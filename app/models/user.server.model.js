var mongoose = require('mongoose'),
    crypto = require('crypto'), 
    Schema = mongoose.Schema;
    
var UserSchema = new Schema({
    created_at: {
        type: Date,
        default: Date.now
    },
    updated_at: Date,
    deleted_at: Date,
    type: {
        type: String,
        default: 'user'
    },
    username: {
        type: String,
        trim: true,
        unique: true
    },
    password: String,
    //authentication
    provider: String,
    providerId: String,
    providerData: {},
    
    profile: {
        given_name: String,
        family_name: String,
        birthday: Date,
        email: String,
        picture: String,
        description: String,
        points: {
            type: Number,
            default: 0
        }
    },
    
    learnedNotes: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Note'
    }],
    
    rank: {
        title: {
            type: String,
            default: "Groene rank"
        },
        value: {
            type: Number,
            default: 1
        },
        image: String
    },
    
    badges: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Badge'
    }],
    
    level: {
        name: {
            type: Number,
            default: 1
        },
        points: {
            type: Number,
            default: 30
        }
    }
});

UserSchema.pre('save',
    function(next) {
        if (this.password) {
            var md5 = crypto.createHash('md5');
            this.password = md5.update(this.password).digest('hex');
        }
        
        next();
    }
);

UserSchema.methods.authenticate = function(password) {
    var md5 = crypto.createHash('md5');
    md5 = md5.update(password).digest('hex');
    
    return this.password === md5;
}

UserSchema.statics.findUniqueUsername = function(username, suffix, callback) {
    var _this = this;
    var possibleUsername = username + (suffix || '');
    
    _this.findOne(
        {username: possibleUsername},
        function(err, user) {
            if(!err) {
                if(!user) {
                    callback(possibleUsername);
                }
                else {
                    return _this.findUniqueUsername(username, (suffix || 0) + 1, callback);
                }
            }
            else {
                callback(null);
            }
        }
    );
};

mongoose.model('User', UserSchema);