var mongoose = require('mongoose'),
    crypto = require('crypto'), 
    Schema = mongoose.Schema;
    
var TestmomentSchema = new Schema({
    
        username: String,
        date:  {
            type: Date,
            expires: 0
        },
        startHour: String,
        endHour: String,
        personsAllowed: Number,
        users: [{
            username: String
        }]
});

mongoose.model('Testmoment', TestmomentSchema);