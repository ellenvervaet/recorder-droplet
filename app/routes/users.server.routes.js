var users = require('../../app/controllers/users.server.controller'),
    passport = require('passport');

module.exports = function(app) {
    
    //dit verwijst naar de functies in users.server.controller.js
    app.route('/api/users')
        .post(users.create)
        .get(users.list);
        
    app.route('/api/users/:userId')
        .get(users.read)
        .put(users.update)
        .delete(users.delete);
    app.param('userId', users.userByID);
    
    app.route('/api/usersByName/:userName')
        .get(users.read)
        .put(users.update);
    app.param('userName', users.userByUsername);
    
    app.route('/register')
        .get(users.renderRegister)
        .post(users.register);
        
    app.route('/login')
        .get(users.renderLogin);
    //post zit in index
        
    app.get('/logout', users.logout);
    
    app.route('/backoffice')
        .get(users.renderBackoffice);
    app.route('/backoffice-login')
        .get(users.renderBackofficeLogin);
        
    app.post('/backoffice',
        passport.authenticate('local'),
        function(req, res, next){
            if ( req.user.type === 'user'){
                res.send(401, 'Not an admin user');
            }
            else{
                next();
            }
        },
        function(req, res) {
            
            // Dit is eigenlijk een kopie van de index.server.controller functie... Niet goed dus
            res.render('Backoffice', {title: 'Backoffice', user : JSON.stringify(req.user)});
            //res.redirect('/');

        });
       
}