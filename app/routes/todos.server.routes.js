var users = require('../../app/controllers/users.server.controller'),
    todos = require('../../app/controllers/todos.server.controller');

module.exports = function(app) {
    
    app.route('/api/todos')
        .get(todos.list)
        //je moet ingelogd zijn om een todo te maken
        .post(users.requiresLogin, todos.create);

    app.route('/api/todos/:todoId')
        .get(todos.read)
        //het moet jouw todo zijn om dit te kunnenn doen
        .put(/*users.requiresLogin, todos.hasAuthorization, */todos.update)
        .delete(users.requiresLogin, todos.hasAuthorization, todos.delete);

    app.param('todoId', todos.todoByID);
};
