module.exports = function(app) {
    var index = require('../controllers/index.server.controller');
    var backoffice = require('../controllers/backoffice.server.controller');
    var passport = require('passport');
    
    app.get('/', index.render);
    app.get('/backoffice', backoffice.render);
    app.post('/',
        passport.authenticate('local'),
        function(req, res) {
            // Dit is eigenlijk een kopie van de index.server.controller functie... Niet goed dus
            res.render('index', {title: 'MEAN MVC', user : JSON.stringify(req.user)});
            //res.redirect('/');
        });
};