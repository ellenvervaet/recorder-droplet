var chatUsers = require('../../app/controllers/chatUsers.server.controller');

module.exports = function(app) {
    
    app.route('/api/chatUsers/')
        .get(chatUsers.list)
        //je moet ingelogd zijn om een note te maken
        ;

    app.route('/api/chatUsers/:chatUserName')
        .get(chatUsers.read)
        .delete(chatUsers.delete)
        .post(chatUsers.create);

    app.param('chatUserName', chatUsers.chatUserByUsername);
};
