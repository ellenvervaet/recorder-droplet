var users = require('../../app/controllers/users.server.controller'),
    studies = require('../../app/controllers/studies.server.controller');

module.exports = function(app) {
    
    app.route('/api/studies/')
        .get(studies.list)
        //je moet ingelogd zijn om een note te maken
        .post(studies.create);

    app.route('/api/studies/:studyId')
        .get(studies.read)
        .put(studies.update)
        .delete(/*users.requiresLogin, studies.hasAuthorization,*/ studies.delete);

    app.param('studyId', studies.studyByID);
    
    app.route('/api/studies-rank/:rank')
        .get(studies.read);

    app.param('rank', studies.studiesByRank);
};
