var User = require('mongoose').model('User'),
    passport = require('passport');
    
//CRUD
//=================================
exports.create = function(req, res, next) {
    var user = new User(req.body);
    user.save(function(err) {
        if (err) {
            return next(err);
        }
        else {
            res.json(user);
        }
    })
}

exports.list = function(req, res, next) {
    User.find({}, function(err, users) {
        if (err) {
            return next(err);
        }
        else {
            res.json(users);
        }
    });
};
//responds with a json representation of the req.user object
exports.read = function(req, res) {
    res.json(req.user);
};

exports.userByID = function(req, res, next, id) {
    User.findOne({
        _id: id
    },
    function(err, user) {
        if (err) {
            return next(err);
        }
        else{
            req.user = user;
            next();
        }
    });
};

exports.userByUsername = function(req, res, next, value) {
    User.findOne({
        username: value
    },
    function(err, user) {
        if (err) {
            return next(err);
        }
        else{
            req.user = user;
            next();
        }
    });
};

exports.update = function(req, res, next) {
    //{new: true} moet erbij indien je als response het aangepaste model wil krijgen
    //anders geeft hij het oorspronklijke document terug.
    User.findByIdAndUpdate(req.user.id, req.body, {new: true}, function(err, user) {
        if (err) {
            return next(err);
        }
        else {
            res.json(user);
        }
    })
};

exports.delete = function(req, res, next) {
    req.user.remove(function(err) {
        if (err) {
            if (err) {
                return next(err);
            }
            else {
                res.json(req.user);
            }
        }
    })
};

//Login, Register and Backoffice
//=========================
var getErrorMessage = function(err) {
    var message = '';
    if (err.code) {
        switch (err.code) {
            case 11000:
            case 11001:
                message = 'Username already exists';
                break;
        }
    }
    else {
        for (var errName in err.errors) {
            if (err.errors[errName].message)
                message = err.errors[errName].message;
        }
    }
    
    return message;
};

exports.renderLogin = function(req, res, next) {
    if (!req.user) {
        res.render('login', {
            title: 'log-in Form',
            messages: req.flash('error') || req.flash('info')
        });
    }
    else {
        return res.redirect('/');
    }
};

exports.renderRegister = function(req, res, next) {
    if (!req.user) {
        res.render('register', {
            title: 'Register Form',
            messages: req.flash('error')
        });
    }
    else{
        return res.redirect('/');
    }
};

exports.register = function(req, res, next) {
    if (!req.user) {
        var user = new User(req.body);
        var message = null;
        //fill standard data
        user.provider = 'local';
        
        user.save(function(err) {
            if (err) {
                var message = getErrorMessage(err);
                req.flash('error', message);
                return res.redirect('/register');
            }
            
            req.login(user, function(err) {
                if (err)
                    return next(err);
                    
                return res.redirect('/');
            });
        });
    }
    else {
        return res.redirect('/');
    }
};

exports.logout = function(req, res) {
    req.logout();
    res.redirect('/');
}

exports.requiresLogin = function(req, res, next) {
    if (!req.isAuthenticted()) {
        return res.status(401).send({
            message: 'User is not logged in'
        });
    }
    next();
};

exports.renderBackoffice = function(req, res, next) {
    if (!req.user) {
        res.render('Backoffice', {
            title: 'Backoffice',
            messages: req.flash('error') || req.flash('info')
        });
    }
    if (req.user.admin == true) {
        return res.redirect('/backoffice');
    }
};

exports.renderBackofficeLogin = function(req, res, next) {
    if (!req.user) {
        res.render('backoffice-login', {
            title: 'log-in Form',
            messages: req.flash('error') || req.flash('info')
        });
    }
    else {
        return res.redirect('/backoffice');
    }
};