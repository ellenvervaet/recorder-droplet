var Rank = require('mongoose').model('Rank');
    
//CRUD
//=================================
exports.create = function(req, res, next) {
    var rank = new Rank(req.body);
    rank.save(function(err) {
        if (err) {
            return next(err);
        }
        else {
            res.json(rank);
        }
    })
}

exports.list = function(req, res, next) {
    Rank.find({}, function(err, ranks) {
        if (err) {
            return next(err);
        }
        else {
            res.json(ranks);
        }
    });
};
//responds with a json representation of the req.rank object
exports.read = function(req, res) {
    res.json(req.rank);
};

exports.rankByID = function(req, res, next, id) {
    Rank.findOne({
        _id: id
    },
    function(err, rank) {
        if (err) {
            return next(err);
        }
        else{
            req.rank = rank;
            next();
        }
    });
};

exports.ranksByValue = function(req, res, next, value) {
  Rank.find({
        value: value
    },
    function(err, rank) {
        if (err) {
            return next(err);
        }
        else{
            req.rank = rank;
            next();
        }
    });
};

exports.update = function(req, res, next) {
    Rank.findByIdAndUpdate(req.rank.id, req.body, {new: true}, function(err, rank) {
        if (err) {
            return next(err);
        }
        else {
            res.json(rank);
        }
    })
};

exports.delete = function(req, res, next) {
    req.rank.remove(function(err) {
        if (err) {
            if (err) {
                return next(err);
            }
            else {
                res.json(req.rank);
            }
        }
    })
};