//Als we niet ingelogd zijn, komen we op de inlogpagina
//anders gaan we naar de home
exports.render = function(req, res) {

    if (!req.user) {
        res.redirect('/backoffice-login');
    }
    else {
        res.render('/backoffice', {
            user: JSON.stringify(req.user)
        })
    }
    
};
