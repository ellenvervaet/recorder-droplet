var ChatUser = require('mongoose').model('ChatUser');
    
//CRUD
//=================================
exports.create = function(req, res, next) {
    var chatUser = new ChatUser(req.body);
    chatUser.save(function(err) {
        if (err) {
            return next(err);
        }
        else {
            res.json(chatUser);
        }
    })
}

exports.list = function(req, res, next) {
    ChatUser.find({}, function(err, chatUsers) {
        if (err) {
            return next(err);
        }
        else {
            res.json(chatUsers);
        }
    });
};
//responds with a json representation of the req.chatUser object
exports.read = function(req, res) {
    res.json(req.chatUser);
};

exports.chatUserByID = function(req, res, next, id) {
    ChatUser.findOne({
        _id: id
    },
    function(err, chatUser) {
        if (err) {
            return next(err);
        }
        else{
            req.chatUser = chatUser;
            next();
        }
    });
};

exports.chatUserByUsername = function(req, res, next, value) {
    console.log(value);
    ChatUser.findOne({
        username: value
    },
    function(err, chatUser) {
        if (err) {
            return next(err);
        }
        else{
            console.log(chatUser);
            req.chatUser = chatUser;
            next();
        }
    });
};

exports.delete = function(req, res, next) {
    console.log('deleting: ');
    console.log(req.chatUser);
    req.chatUser.remove(function(err) {
        if (err) {
            return next(err);
        }
        else {
            res.json(req.chatUser);
        }
        
    })
};