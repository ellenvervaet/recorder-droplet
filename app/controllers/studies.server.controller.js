var Study = require('mongoose').model('Study');
    
//CRUD
//=================================
exports.create = function(req, res, next) {
    var study = new Study(req.body);
    study.save(function(err) {
        if (err) {
            return next(err);
        }
        else {
            res.json(study);
        }
    })
}

exports.list = function(req, res, next) {
    Study.find({}, function(err, studies) {
        if (err) {
            return next(err);
        }
        else {
            res.json(studies);
        }
    });
};
//responds with a json representation of the req.study object
exports.read = function(req, res) {
    res.json(req.study);
};

exports.studyByID = function(req, res, next, id) {
    Study.findOne({
        _id: id
    },
    function(err, study) {
        if (err) {
            return next(err);
        }
        else{
            req.study = study;
            next();
        }
    });
};

exports.studiesByRank = function(req, res, next, value) {
  Study.find({
        rank: { $lt: value }
    },
    function(err, study) {
        if (err) {
            return next(err);
        }
        else{
            req.study = study;
            next();
        }
    });
};

exports.update = function(req, res, next) {
    Study.findByIdAndUpdate(req.study.id, req.body, {new: true}, function(err, study) {
        if (err) {
            return next(err);
        }
        else {
            res.json(study);
        }
    })
};

exports.delete = function(req, res, next) {
    req.study.remove(function(err) {
        if (err) {
            if (err) {
                return next(err);
            }
            else {
                res.json(req.study);
            }
        }
    })
};