;(function() {
    'use strict';
    
    angular.module('app_backoffice.studies')
    
    .controller('StudiesController', StudiesController);
    
    //StudiesController.$inject['$scope', '$routeParams', '$location', 'Authentication', 'Studies', '$rootScope', 'NoteService'];
    
    function StudiesController($scope, Studies, $routeParams, $location) {
        $scope.menuTemplate = "templates/backend/partials/menu.html";
        
        $scope.create = function() {
            
            var instruments = {}
        
            if(this.instruments.sopranino == true)
                instruments.sopranino = { "name" : "Sopranino"}
            
            var study = new Studies({                
                name : this.name,
                author : this.author,
                description : this.description,
                rank : this.rank,
                genre : this.genre,
                focus : this.focus,
                time:  this.time,
                key : this.key,
                notes : this.notes,
                perfectDuration : this.perfectDuration, 
                maxPoints : this.maxPoints, 
                instruments,
                checklist : {
                    tempo : this.checklist.tempo,
                    notes : this.checklist.notes
                }
            });

            study.$save(function(response) {
                $location.path('/studies');
            }, function(errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };

        $scope.find = function() {
            $scope.studies = Studies.query();
        };

        $scope.findOne = function() {
            $scope.study = Studies.get({
                studyId: $routeParams.studyId
            });
        };

        $scope.update = function() {
            $scope.study.$update(function() {
                $location.path('studies');
            }, function(errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };

        $scope.delete = function(study) {
            if (study) {
                $scope.study = Studies.get({
                    studyId: study
                }).$promise.then(function(study){
                    study.$remove(function() {
                        for (var i in $scope.studies) {
                            if ($scope.studies[i] === study) {
                                $scope.studies.splice(i, 1);
                            }
                        }
                    });
                    $location.path('/studies');
                });
                
            } 
        };
    }
})();
