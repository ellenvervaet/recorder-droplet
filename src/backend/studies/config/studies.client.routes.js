;(function() {
    'use strict';
    angular.module('app_backoffice.studies')
    .config(Routes);
    
    //Routes.$inject['$routeProvider'];
    
    function Routes($routeProvider) {
        
        $routeProvider.
        when('/studies', {
            templateUrl: 'templates/backend/studies/views/studies.client.view.html',
            controller: 'StudiesController'
        })
        .when('/studies/:studyId/edit', {
            templateUrl: 'templates/backend/studies/views/study-edit.client.view.html',
            controller: 'StudiesController'
        })
        .when('/studies/create', {
            templateUrl: 'templates/backend/studies/views/study-create.client.view.html',
            controller: 'StudiesController'
        })
        .when('/studies/:studyId', {
            templateUrl: 'templates/backend/studies/views/study-details.client.view.html',
            controller: 'StudiesController'
        });
        
    }
})();