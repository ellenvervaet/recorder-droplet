;(function() {
    'use strict';
    angular.module('app_backoffice.studies')
    .factory('Studies', Studies);
    
    //Studies.$inject['$resource'];
       
    function Studies($resource) {
        return $resource('api/studies/:studyId', {
            studyId: '@_id'
        }, {
            update: {
                method: 'PUT', 
            }
        });
    }
    

   
})();