;(function() {
    'use strict';
    angular.module('app_backoffice.home')
    .config(Routes);
    
    //Routes.$inject['$routeProvider'];
    
    function Routes($routeProvider) {
        
        $routeProvider.
        when('/', {
            templateUrl: 'templates/backend/home/views/home.client.view.html',
            controller: 'HomeController'
        });
        
    }
})();