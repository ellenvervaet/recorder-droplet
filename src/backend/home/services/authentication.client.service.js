;(function() {
    'use strict';
    angular.module('app_backoffice.home')
    .factory('Users', Users)
    .factory('Authentication', Authentication);
    
    //Users.$inject['$resource'];
    
    function Authentication() {
        this.user = window.user;
        
        return{
            user: this.user  
        }; 
    }
    
    function Users($resource) {
        return $resource('api/users/:userId', {
            userId: '@_id'
        }, {
            update: {
                method: 'PUT'
            }
        });
    }
    

   
})();