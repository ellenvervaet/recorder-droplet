;(function() {
    'use strict';
    
    angular.module('app_backoffice.recordings')
    
    .controller('RecordingsController', RecordingsController);
    
    //RecordingsController.$inject['$scope', '$routeParams', '$location', 'Authentication', 'Recordings', '$rootScope', 'NoteService'];
    
    function RecordingsController($scope, Recordings, $routeParams, $location, $sce) {
        $scope.menuTemplate = "templates/backend/partials/menu.html";
                
        $scope.create = function() {
            
            var instruments = {}
        
            if(this.instruments.sopranino == true)
                instruments.sopranino = { "name" : "Sopranino"}
            
            var recording = new Recordings({                
                name : this.name,
                author : this.author,
                description : this.description,
                rank : this.rank,
                genre : this.genre,
                focus : this.focus,
                time:  this.time,
                key : this.key,
                notes : this.notes,
                perfectDuration : this.perfectDuration, 
                maxPoints : this.maxPoints, 
                instruments,
                checklist : {
                    tempo : this.checklist.tempo,
                    notes : this.checklist.notes
                }
            });

            recording.$save(function(response) {
                $location.path('/recordings');
            }, function(errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };

        $scope.find = function() {
            $scope.recordings = Recordings.query();
        };

        $scope.findOne = function() {
            $scope.recording = Recordings.get({
                recordingId: $routeParams.recordingId
            }).$promise.then(function(audio){
                var audioElem = document.createElement("audio");
                audioElem.setAttribute("controls", "controls");
                var src = document.createElement("source");
                src.setAttribute('src', trustAudio(audio.data));
                src.setAttribute('type', 'audio/ogg');
                audioElem.appendChild(src);
                var element = document.getElementById('audio');
                element.appendChild(audioElem); 
            
                $scope.recording = audio;
                function trustAudio(audio) {
                    return $sce.trustAsResourceUrl(audio);
                }
            });
            
            
        };

        $scope.update = function() {
            $scope.recording.$update(function() {
                $location.path('recordings');
            }, function(errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };

        $scope.delete = function(recording) {
            if (recording) {
                $scope.recording = Recordings.get({
                    recordingId: recording
                }).$promise.then(function(recording){
                    recording.$remove(function() {
                        for (var i in $scope.recordings) {
                            if ($scope.recordings[i] === recording) {
                                $scope.$apply(function(){
                                    $scope.recordings.splice(i, 1);
                                });
                            }
                        }
                    });
                    $location.path('/recordings');
                });
            } 
        };
        
        $scope.deleteFeedback = function(recording_id, feedback){
            if (recording_id) {
                $scope.recording = Recordings.get({
                    recordingId: recording_id
                }).$promise.then(function(recording){
                   
                    for (var i in recording.feedbacks) {
                         console.log(recording.feedbacks[i]);
                         console.log(feedback);
                        if (recording.feedbacks[i].body == feedback.body
                             && recording.feedbacks[i].username == feedback.username) {
                            recording.feedbacks.splice(i, 1);
                            console.log('sliced');
                        }
                    }
                    $scope.$apply(function(){
                        $scope.recording.feedbacks = recording.feedbacks;
                        });
                    recording.$update(function() {
                        $location.path('recordings/' + recording_id);
                    }, function(errorResponse) {
                        $scope.error = errorResponse.data.message;
                    });
                });
            } 
        }
    }
})();
