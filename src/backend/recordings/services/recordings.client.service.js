;(function() {
    'use strict';
    angular.module('app_backoffice.recordings')
    .factory('Recordings', Recordings);
    
    //Recordings.$inject['$resource'];
       
    function Recordings($resource) {
        return $resource('api/recordings/:recordingId', {
            recordingId: '@_id'
        }, {
            update: {
                method: 'PUT', 
            }
        });
    }
    

   
})();