;(function() {
    'use strict';
    angular.module('app_backoffice.recordings')
    .config(Routes);
    
    //Routes.$inject['$routeProvider'];
    
    function Routes($routeProvider) {
        
        $routeProvider.
        when('/recordings', {
            templateUrl: 'templates/backend/recordings/views/recordings.client.view.html',
            controller: 'RecordingsController'
        })
        .when('/recordings/:recordingId/edit', {
            templateUrl: 'templates/backend/recordings/views/recording-edit.client.view.html',
            controller: 'RecordingsController'
        })
        .when('/recordings/create', {
            templateUrl: 'templates/backend/recordings/views/recording-create.client.view.html',
            controller: 'RecordingsController'
        })
        .when('/recordings/:recordingId', {
            templateUrl: 'templates/backend/recordings/views/recording-details.client.view.html',
            controller: 'RecordingsController'
        });
        
    }
})();