;(function() {
    'use strict';
    
    angular.module('app_backoffice.users')
    
    .controller('UsersController', UsersController);
    
    //UsersController.$inject['$scope', '$routeParams', '$location', 'Authentication', 'Users', '$rootScope', 'NoteService'];
    
    function UsersController($scope, Users, $routeParams, $location) {
        $scope.menuTemplate = "templates/backend/partials/menu.html";
        
        $('.datepicker').datepicker({yearRange: "-80:+0", changeYear: true, dateFormat: 'dd-mm-yy' });
        
        $scope.create = function() {
            var user = new Users({
                username: this.username,
                password: this.password,
                profile: {
                    given_name: this.profile.given_name,
                    family_name: this.profile.family_name,
                    email: this.profile.email,
                    birthday: $("#birth-backoffice").datepicker('getDate')                   
                }
            });

            user.$save(function(response) {
                $location.path('/users');
            }, function(errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };

        $scope.find = function() {
            $scope.users = Users.query();
        };
        
        $scope.findOne = function() {
            $scope.user = Users.get({
                userId: $routeParams.userId
            });
        };

        $scope.update = function() {
            $scope.user.$update(function() {
                $location.path('users');
            }, function(errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };

        $scope.delete = function(user) {
            if (user) {
                $scope.user = Users.get({
                    userId: user
                }).$promise.then(function(user){
                    user.$remove(function() {
                        for (var i in $scope.users) {
                            if ($scope.users[i] === user) {
                                $scope.$apply(function(){
                                    $scope.users.splice(i, 1);
                                });
                            }
                        }
                    });
                    $location.path('/users');
                });
                
            } 
        };
    }
})();
