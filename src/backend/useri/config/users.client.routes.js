;(function() {
    'use strict';
    angular.module('app_backoffice.users')
    .config(Routes);
    
    //Routes.$inject['$routeProvider'];
    
    function Routes($routeProvider) {
        
        $routeProvider.
        when('/users', {
            templateUrl: 'templates/backend/useri/views/users.client.view.html',
            controller: 'UsersController'
        })
        .when('/users/:userId/edit', {
            templateUrl: 'templates/backend/useri/views/user-edit.client.view.html',
            controller: 'UsersController'
        })
        .when('/users/create', {
            templateUrl: 'templates/backend/useri/views/user-create.client.view.html',
            controller: 'UsersController'
        })
        .when('/users/:userId', {
            templateUrl: 'templates/backend/useri/views/user-details.client.view.html',
            controller: 'UsersController'
        });
        
    }
})();