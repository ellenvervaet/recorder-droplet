;(function() {
    'use strict';
    angular.module('app_backoffice.users')
    .factory('Users', Users);
    
    //Users.$inject['$resource'];
       
    function Users($resource) {
        return $resource('api/users/:userId', {
            userId: '@_id'
        }, {
            update: {
                method: 'PUT', 
            }
        });
    }
    

   
})();