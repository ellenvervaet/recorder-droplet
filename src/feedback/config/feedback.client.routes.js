;(function() {
    'use strict';
    angular.module('app.feedback')
    .config(Routes);
    
    function Routes($routeProvider) {
        
        $routeProvider.
        when('/feedback', {
            templateUrl: 'templates/feedback/views/feedback.client.view.html',
            controller: 'FeedbackController'
        });
        
    }
})();

