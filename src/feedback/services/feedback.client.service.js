;(function() {
    'use strict';
    
    angular.module('app.feedback')
    
    .factory('FeedbackRecordings', FeedbackRecordings)
    .factory('Recordings', Recordings);
    
    //Recordings.$inject['$resource'];
    
    function FeedbackRecordings($resource) {
        return $resource('api/feedback-recordings/:feedbackId', {
        });
    }
    function Recordings($resource){
        return $resource('api/recordings/:recordingId', {
            recordingId: '@_id'
        }, {
            update: {
                method: 'PUT'
            }
        });
    }
    
})();
