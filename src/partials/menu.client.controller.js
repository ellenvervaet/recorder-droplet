;(function() {
    'use strict';
    
    angular.module('app.studies')
    
    .controller('MenuCtrl', MenuCtrl);
    
    //PickAStudyController.$inject['$scope', '$routeParams', '$location', 'Authentication'];
     
    function MenuCtrl($scope, NoteService, $rootScope) {
        NoteService.CountLearnedNotes(true, $scope.user).then(
            function(response){$scope.altNotes = response
        });
        NoteService.CountLearnedNotes(false, $scope.user).then(
            function(response){$scope.descentNotes = response
        });
        
        //percentage berekenen punten level
        var fullwidth = $scope.user.level.points;
        var earnedWidth = $scope.user.profile.points; 
        $scope.earnedPercent = earnedWidth/fullwidth*100;
        
        $( document ).ready(function() {
            $('#percent').width($scope.earnedPercent + '%');
        });
    }
    
})();
