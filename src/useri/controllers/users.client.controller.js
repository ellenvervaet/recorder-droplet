;(function() {
    'use strict';
    
    angular.module('app.users')
    
    .controller('UsersController', UsersController);
    
    //UsersController.$inject['$scope', '$routeParams', '$location', 'Authentication', 'Users', '$rootScope', 'NoteService'];
    
    function UsersController($scope, $routeParams, $location, Authentication, Users, $rootScope, NoteService) {
        //de aangemelde gebruiker
        if(!$rootScope.user){
            //dit is de gebruiker die we krijgen bij het aanmelden
            //wanneer we deze zouden updaten is deze niet meer relevant
            $scope.user = $rootScope.user = Authentication.user;
        }else{
            $scope.user = $rootScope.user;
            $scope.earnedPoints = $rootScope.earnedPoints;
            $scope.learnedNote = $rootScope.learnedNote;
        }
         $rootScope.user = $scope.user;
        //laadt het menu
        $scope.menuTemplate = "templates/partials/menu.html";     

        $scope.update = function() {
            $scope.user.$update(function() {
                $rootScope.user = $scope.user;
                $location.path('/');
            }, function(errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };
    }
})();
