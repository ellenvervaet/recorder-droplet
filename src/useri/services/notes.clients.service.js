;(function() {
    'use strict';
    angular.module('app.users')
    .factory('Notes', Notes)
    .service('NoteService', NoteService);
    
    //Notes.$inject['$resource'];
    
    function Notes($resource) {
        return $resource('api/notes/:noteId', {
            userId: '@_id'
        });
    }
    
    function NoteService(Notes, $q){
        var service = {
            CheckIfNoteLearned: CheckIfNoteLearned,
            CountLearnedNotes: CountLearnedNotes
        };
        
        return service; 
            
        function CheckIfNoteLearned(note, user){
            if(user.learnedNotes.indexOf(note._id) !== -1){
                return true;
            }else{
                return false;
            }
        }            
        function CountLearnedNotes(treble, user){
            return $q( function(resolve, reject) {
                Notes.query().$promise.then(function(notes){
                    var counter = 0;
                    
                    notes.forEach(function(note){
                        if(note.treble == treble){
                            if(CheckIfNoteLearned(note, user)){
                               counter++;
                            }
                        }
                    })
                    resolve(counter);
                })
            })
        }   
    }
    
        
})();