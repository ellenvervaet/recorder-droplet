;(function() {
    'use strict';
    angular.module('app.users')
    .factory('Levels', Levels)
    .factory('Ranks', Ranks)
    .factory('Gamification', Gamification);
    
    function Levels($resource) {
        return $resource('api/levels/:name', {
            name: '@name'
        });
    }
    function Ranks($resource){
        return $resource('/api/ranks/:rankValue', {
            rankValue: '@value'
        },{
            query: {
                method:'GET', isArray:true
            } });
    }
    
    function Gamification(Levels, $q, $rootScope, UserByName, Ranks){
        
        var service = {
            checkForLevelUp: checkForLevelUp,
            checkForRankUp: checkForRankUp
        };
        
        return service;
        
        function checkForLevelUp(user, earnedpoints){
            $rootScope.earnedPoints = earnedpoints;
            
            return $q( function(resolve, reject) {
                
                var totalpoints = user.profile.points + earnedpoints;
                
                if((totalpoints) >= user.level.points){
                                        
                    var leftOvers = totalpoints - user.level.points;
                    
                    Levels.query({
                        name: user.level.name + 1
                    }).$promise.then(function(response){                        
                        //fill in new data                            
                        user.profile.points = leftOvers;
                        user.level.name = response[0].name;
                        user.level.points = response[0].points;            

                        resolve(user);      
                    }).catch(function(err){console.log(err)})
                    
                }else{
                    user.profile.points = totalpoints;
                    resolve(user); 
                }
            })
        }
        
        function checkForRankUp(username, earnedpoints){
            
            return $q( function(resolve, reject) {
                if(earnedpoints >= 50){
                    UserByName.get({
                        userName: username
                    }).$promise.then(function(user){
                        console.log(user);                        
                        var nextRank = user.rank.value + 1;          
                        Ranks.query({
                            rankValue: nextRank
                        }).$promise.then(function(rank){
                            user.rank = rank[0];
                            console.log('user: ');
                            console.log( user);
                            console.log('newRank: ');
                            console.log( rank);
                            user.$update(function(response){
                                console.log('success: ' );
                                console.log( response);
                            }, function(errorResponse) {
                                console.log(errorResponse);
                            });
                        })
                        resolve(user);      
                    }).catch(function(err){console.log(err)})
                }
                else{
                    resolve('Te lage score');
                }
            })
        }
    }
    
})();