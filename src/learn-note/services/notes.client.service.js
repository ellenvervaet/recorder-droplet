;(function() {
    'use strict';
    
    angular.module('app.notes')
    .factory('RankedNotes', RankedNotes);
    
    function RankedNotes($resource) {
        return $resource('api/notes-rank/:rank', {
            rank: '@rank'
        });
    }
    
})();