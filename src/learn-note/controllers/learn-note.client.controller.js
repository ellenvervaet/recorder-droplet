;(function() {
    'use strict';
    
    angular.module('app.notes')    
    .controller('LearnANoteController', LearnANoteController);
        
    //PickANoteController.$inject['$scope', '$routeParams', '$location', 'Notes', '$rootScope'];
    
    function LearnANoteController($scope, $routeParams, $location, Notes, $rootScope, Users, Pitcher, Gamification, NoteService){
        //laadt het menu
        $scope.user = $rootScope.user;
        $scope.menuTemplate = "templates/partials/menu.html";
              
        //CRUD
        var note;
        
        $scope.findOne = function() {
            $scope.note = Notes.get({
                noteId: $routeParams.noteId
            }).$promise.then(function(data){
                $scope.tips = data.tips;
                if(NoteService.CheckIfNoteLearned(data, $scope.user) == true){
                    $('h4.rulesLearnNote').text("Je hebt de " + data.name.toLowerCase() + " al aangeleerd, goed gedaan!");
                    $('p.rulesLearnNote').text("Je kan de noot nog steeds oefenen maar er zal niet meer afgeteld worden.");
                }
                setScoreAndListen(data);})};
            
        function setScoreAndListen(response){
            note = response;
            $('.exampleNoteDiv h2').html(note.name);
            Pitcher.fillExampleField(note, "#exampleNote");
            $('img.example').attr('src', note.finger);
            
            //vraag toestemming voor de microfoon
            navigator.mediaDevices.getUserMedia({audio: true, video: false})
            .then(function(audioStream){
                $scope.stream = audioStream;
                reviseAndCountDown(audioStream)});}
                
        function reviseAndCountDown(mediaStream){
            //luister en tel af
            Pitcher.noteToLearn(note);
            Pitcher.gotStream(mediaStream);
            Pitcher.updatePitch(note);
            if (!NoteService.CheckIfNoteLearned(note, $scope.user) == true){
                Pitcher.myVar(setInterval(
                    reviser, 1000));}}
        
        function reviser(){
            Pitcher.meetDoel()
                .then(function(){  
                    fillUserData();});}
        
        function fillUserData(){
            $rootScope.learnedNote = note.name;
            $scope.user.learnedNotes.push(note._id);
            $scope.user = Gamification.checkForLevelUp($scope.user, note.points)
            .then(function(updatedUser){
                console.log('after gamification check');
                console.log(updatedUser);
                updateUserAndGoHome(updatedUser);});}
        
        function updateUserAndGoHome(updatedUser){
            Users.get({
                userId: $rootScope.user._id
            }).$promise.then(function(user){
                user.profile = updatedUser.profile;
                user.level = updatedUser.level;
                user.learnedNotes.push(note._id);
                $rootScope.user = updatedUser;
                user.$update(function() {
                        $location.path('/');
                    }, function(errornote) {
                        $scope.error = errornote.data.message;
                    }
                )}
            );
        }
    }
    
})();   