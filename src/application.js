;(function () {
    'use strict';
    
    var app = angular.module('app', [
        'ngResource', 
        'ngRoute', 
        'ngDialog',
        'app.users',
        'pubnub.angular.service',
        'app.notes',
        'app.studies',
        'app.recordings',
        'app.tests',
        'app.feedback',
        'app.backoffice'
        ]);
        
    angular.module('app.users', []);
    angular.module('app.notes', []);
    angular.module('app.studies', []);
    angular.module('app.recordings', []);
    angular.module('app.feedback', []);
    angular.module('app.tests', []);
    angular.module('app.backoffice', []);
    

    app.config(['$locationProvider', function($locationProvider) {
        $locationProvider.hashPrefix('!');
        }
    ]);
    
    var app_backoffice = angular.module('app_backoffice', [
        'ngResource', 
        'ngRoute', 
        'app_backoffice.home',
        'app_backoffice.users',
        'app_backoffice.studies',
        'app_backoffice.recordings',
    ]);
    
    angular.module('app_backoffice.home', []);
    angular.module('app_backoffice.users', []);
    angular.module('app_backoffice.studies', []);
    angular.module('app_backoffice.recordings', []);
    
    app_backoffice.config(['$locationProvider', function($locationProvider) {
        $locationProvider.hashPrefix('!');
        }
    ]);
    
    if (window.location.hash === '#_=_')
        window.location.hash = '#!';

    angular.element(document).ready(function(){
        angular.bootstrap(document, 'app');
    });

})();
