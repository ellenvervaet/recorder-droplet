;(function() {
    'use strict';
    
    angular.module('app.tests')
    
    .controller('TestController', TestController);
    
    //PickAStudyController.$inject['$scope', '$routeParams', '$location', 'Authentication'];
     
    function TestController($timeout, $scope, $rootScope, $location, ChatUsers, ngDialog, TestRecordings, Recordings, Audio, Pubnub, Realtime, VidChat, Gamification, TestMoments) {
        var head= document.getElementsByTagName('head')[0];
        var script= document.createElement('script');
        
        //laadt het menu
        $scope.user = $rootScope.user
        $scope.menuTemplate = "templates/partials/menu.html";
        
        $scope.status = "notLoggedIn";
        
        Realtime.init();
        
        //REALTIME CHECKLIST
        //De checklist die dient om te quoteren als er een toets afgenomen wordt
        $scope.checklist = {};
        
        var checkChannel = 'checklist-channel';
        Realtime.subscribeToChannel(checkChannel);
        $scope.watchChecklist = function(){
            var list = $scope.checklist;
            var count = 0;
            for (var i in list) {
                if (list.hasOwnProperty(i)) {
                ++count;
                }
            }
            if(count >= 4){
                $scope.checklist.result = list.result = list.tempo + list.rythm + list.notes + list.musicality;
            }
            Realtime.sendMessage(list, checkChannel);
        }
        $scope.$on(Pubnub.getMessageEventNameFor(checkChannel), function (ngEvent, m) {
            $scope.$apply(function () {
                $scope.checklist = m;
            });
        });
       
       //REALTIME ONLINE KOMEN
       //Gebruikers die on/offline gaan voor een toets wordt realtime weergegeven
        var channel = 'chatUser-channel';
        Realtime.subscribeToChannel(channel);
        var object = {
                        username: $scope.user.username,
                        delete: false
                    };
        $scope.sendMessage = function(){
            Realtime.sendMessage(object, channel);
        }
        
        $scope.$on(Pubnub.getMessageEventNameFor(channel), function (ngEvent, m) {
            if(m.delete == false){
                delete m.delete; //we willen de delete-prop niet bij de chatUsers pushen
                var exists = false; 
                $scope.chatUsers.forEach(function(chatUser) {
                    if(chatUser.username == m.username){
                        exists = true;
                    }
                });
                if(exists == false){
                    $scope.$apply(function () {
                        $scope.chatUsers.push(m)
                    });
                }
            }
            else{
                var index = 0;
                $scope.chatUsers.forEach(function(chatUser) {
                    if(chatUser.username == m.username){
                        $scope.chatUsers.splice(index, 1);
                        $scope.$apply(function () {
                            $scope.chatUsers;
                        });
                    }
                    index++;
                });
            }
        });
       
       //CRUD
        $scope.find = function() {
            $scope.chatUsers = ChatUsers.query(function(data){
                $scope.chatUsers = data;
            });
        }
        $scope.getRecordings = function(){
            console.log($scope.user._id);
            $scope.recordings = TestRecordings.query({
                userId: $scope.user._id
            }).$promise.then(function(data){
                console.log(data);
                $timeout(function() {
                    console.log(data);
                    $scope.recordings = data;
                });
            });
        }
        $scope.getTestMoments = function(){
            $scope.testMoments = TestMoments.query(function(data){
                $scope.testMoments = data;
            });
        }
        $scope.addTestMoment = function(){
            var testmoment = new TestMoments({
                username: $scope.user.username, 
                date: new Date($scope.testMoment.date),
                startHour: new Date($scope.testMoment.start),
                endHour: new Date($scope.testMoment.end),
                personsAllowed: $scope.testMoment.places
            });
            testmoment.$save(function(response) {
                console.log(response);
            }, function(errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        }
        

        
        
        
        //VIDEOCHAT
        //Je moet een liedje gekozen hebben voor je kan inloggen als je geen gouden rank bent
        $scope.checkLogin = function() {
            var recording = $('#recordings input[type=radio]:checked').val();
            if(recording !== undefined || $scope.user.rank.title == 'Gouden rank'){
                login();
            }else{
                alert("Selecteer eerst het stuk dat je wilt spelen"); }
        }
        
        var login = function() {
            $scope.status = "loggingIn";
            
            createChatUser();
            
            $scope.phone = VidChat.createPhone($scope.user.username);
            $scope.phone.ready(function(){ 
                $scope.status = "loggedIn";
                $scope.$apply();
            });
            $scope.phone.receive(function(session){
                session.connected(function(session) {
                    var username;
                    //de gouden-rank gebruiker
                    if($scope.chatUsername !== undefined){
                        username = $scope.chatUsername;
                    }else{//diegene die de toets doet
                        username = $scope.chatUser.username;
                    }
                    $scope.chatData = ChatUsers.query({
                        userName: username
                    }).$promise.then(function(chatData){
                        $scope.chatData = chatData[0];
                        openDialog();
                        fillDialog(session);
                        })
                });
                session.ended(function() { $scope.phone.hangup(); });
            });
        }
        
        $scope.makeCall = function (username){
            $scope.chatUsername = VidChat.call(username);}
        
        var openDialog = function(){
            ngDialog.openConfirm({
                template: 'templates/partials/testChat.html',
                className: 'ngdialog-theme-default',
                scope: $scope, // <- ability to use the scopes from directive controller
            }).then(function (success) {
                Gamification.checkForRankUp($scope.chatUsername, $scope.checklist.result);
            }, function (error) {
               
            });
        }
        
        var fillDialog = function(session){
            $rootScope.$on('ngDialog.opened', function (e, $dialog) {
                document.getElementById('incomingVid').appendChild(session.video);
                var thumbDiv = document.getElementById('outgoingVid');
                $scope.phone.addLocalStream(thumbDiv);
                
                Audio.placeTag('audio', $scope.chatData.songData);
            }); 
        }
        
        
        var createChatUser = function() {
            if($scope.user.rank.title !== 'Gouden rank'){
                Recordings.get({
                    recordingId: $('#recordings input[type=radio]:checked').val()
                }).$promise.then(function(recording){
                    fillUserAndScope(recording);
                });
            }
        };
        
        function fillUserAndScope(recording){
            var user = $scope.user;
            VidChat.fillChatUserAndSave(recording, user).then(function(chatUser){
                $scope.chatUser = chatUser;
                $scope.chatUsers.push(chatUser);
                Realtime.sendMessage(object, channel); 
            });
        }
        
        $scope.$on('$locationChangeSuccess', function(){
             object.delete = true;
             Realtime.sendMessage(object, channel); 
             VidChat.logout($scope.user.rank.title, $scope.status, $scope.user.username);
        });
        
        window.addEventListener("beforeunload", function (e) {
            if($location.path() == "/test"){
                object.delete = true;
                Realtime.sendMessage(object, channel); 
                VidChat.logout($scope.user.rank.title, $scope.status, $scope.user.username);
                 
            }
        });
    }
})();
