;(function() {
    'use strict';
    angular.module('app.tests')
    .config(Routes);
    
    function Routes($routeProvider) {
        
        $routeProvider.
        when('/test', {
            templateUrl: 'templates/tests/views/test.client.view.html',
            controller: 'TestController'
        });
        
    }
})();

