;(function() {
    'use strict';
    
    angular.module('app.tests')
    
    .factory('ChatUsers', ChatUsers)
    .factory('TestRecordings', TestRecordings)
    .factory('TestMoments', TestMoments);
    
    //Studies.$inject['$resource'];
    
    function ChatUsers($resource) {
        return $resource('api/chatUsers/:chatUserName', {
            chatUserName: '@username'
        }, {
            'query':  {method:'GET', isArray:true}
        });
    }
    
    function TestRecordings($resource) {
        return $resource('api/testRecordings/:userId', {
            recordingId: '@userId'
        }, {
            'query':  {method:'GET', isArray:true}
        });
    }
    
    function TestMoments($resource){
        return $resource('api/testmoments/', 
         {
            'save':  {method:'POST'}
        });
    }
    
})();
