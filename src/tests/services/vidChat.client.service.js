;(function() {
    'use strict';
    angular.module('app.tests')
    .factory('VidChat', VidChat);
    
    function VidChat(ChatUsers, $q){
       
       
        var service = {
            createPhone: createPhone,
            call: call,
            fillChatUserAndSave: fillChatUserAndSave,
            logout: logout
        };
        
        return service;
       
       function createPhone(username){
            var phone = window.phone = PHONE({
                number        : username || "Anonymous", // listen on username line else Anonymous
                publish_key   : 'pub-c-27dffc2d-ccd3-4674-8413-c4cdcd8703ad',
                subscribe_key : 'sub-c-74d07376-fb25-11e5-8679-02ee2ddab7fe',
            });	
            console.log('phone created');
             var ctrl = window.ctrl = CONTROLLER(phone);
            return ctrl;
        } 
        
        function logout(rankTitle, status, username){
            if(rankTitle !== 'Gouden rank' && status == "loggedIn"){
                ChatUsers.get({
                    chatUserName: username
                }).$promise.then(function(chatUser){
                    console.log(chatUser);
                    chatUser.$remove(function(chatUser){
                    
                    },function(errorResponse) {
                        console.log(errorResponse);
                    });
                });
            }
        }
        
        function call(username){
            if (!window.phone) alert("Login First!");
            else phone.dial(username);
            return username;
        }
        
        function fillChatUserAndSave(recording, user){
            return $q( function(resolve, reject) {
                
                var chatUser = new ChatUsers({
                    username: user.username,
                    currentRank: user.rank.title,
                    songTitle: recording.title,
                    songAuthor: recording.author,
                    songData: recording.data,
                    songChecklist: recording.testChecklist
                });
                        
                chatUser.$save(function(response) {
                    resolve(chatUser);
                }, function(errorResponse) {
                    reject(errorResponse.data.message);
                });
                
            });
        }
        
    }
    
})();