;(function() {
    'use strict';
    angular.module('app.tests')
    .factory('Realtime', Realtime);
    
    function Realtime(Pubnub){
       
        var uuid = Math.floor((Math.random() * 100) + 1); 
       
        var service = {
            init: init,
            subscribeToChannel: subscribeToChannel,
            sendMessage: sendMessage
        };
        
        return service;
       
        function init(){
            Pubnub.init({
                publish_key   : 'pub-c-27dffc2d-ccd3-4674-8413-c4cdcd8703ad',
                subscribe_key : 'sub-c-74d07376-fb25-11e5-8679-02ee2ddab7fe',
                uuid: uuid
            }); 
        }
        
        function subscribeToChannel(channel){
            Pubnub.subscribe({
                channel: channel,
                triggerEvents: ['callback']
            });
        }
        
        function sendMessage(object, channel){
            Pubnub.publish({
                channel: channel,
                message: object,
                callback: function(m) {
                    console.log(m);
                }
            });
        }
    }
    
})();