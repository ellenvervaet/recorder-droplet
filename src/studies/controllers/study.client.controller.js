;(function() {
    'use strict';
    
    angular.module('app.studies')
    
    .controller('StudyController', StudyController);
    
    //PickAStudyController.$inject['$scope', '$routeParams', '$location', 'Authentication'];
     
    function StudyController($scope, $routeParams, $location, $rootScope, Studies, ScorePursuit, ngDialog, Recordings, Gamification, Users) {
        //laadt het menu
        $scope.user = $rootScope.user
        $scope.menuTemplate = "templates/partials/menu.html";
        
        //CRUD
        $scope.findOne = function(){
            $scope.study = Studies.get({
                studyId: $routeParams.studyId
            }).$promise.then(function(study){
                $scope.study = study;
                activate(study);
            });
        }
        
        var recordRTC;
        
        function activate(study){
            var notes = study.notes;
            var key = study.key;
            var time = study.time;
            
            
            
            ScorePursuit.dialogIsClosed();
            ScorePursuit.drawScore(notes, key, time);
            listen();
        }

        function listen(){
            navigator.mediaDevices.getUserMedia({audio: true, video: false})
            .then(function(audioStream){
                var options = {
                        mimeType: 'audio/ogg'
                    };
                recordRTC = RecordRTC(audioStream, options);
                $scope.startRecording = function(){
                    recordRTC.startRecording();
                    console.log('started recording');
                }                
                
                $scope.stopRecording = stopRecording;
                $scope.stream = audioStream;
                
                getReadyForPursuit(audioStream)});}
                
        function getReadyForPursuit(mediaStream){
            ScorePursuit.gotStream(mediaStream);
            ScorePursuit.updatePitch(recordRTC)
            .then(function(data){console.log(data)})
        }
        
        var stopRecording = function(stream){
            $scope.stream.stop();
            
            $scope.totalNotes = ($scope.study.notes.match(/\//g) || []).length;
            $scope.correctNotes = $scope.totalNotes - parseInt($('#fouteNoten').text());
            
            $scope.totalPointsNotes = $scope.study.checklist.notes;
            $scope.pointsNotes = Math.round($scope.correctNotes / $scope.totalNotes * $scope.totalPointsNotes);
                
            recordRTC.stopRecording(function(audioURL) {
                //audio.src = audioURL;
                
                $rootScope.audio = audioURL;
                
                recordRTC.getDataURL(function(dataURL) { 
                    $rootScope.audioURL = dataURL;
                });
            });
            
            ngDialog.openConfirm({
                template: 'templates/partials/studyFinished.html',
                className: 'ngdialog-theme-default',
                scope: $scope, // <- ability to use the scopes from directive controller
            }).then(function (success) {
                console.log('close with save');
                $scope.studyDescription = $('#studyDescription').val();
                
                recordRTC.getDataURL(function(dataURL) { 
                    var recording = new Recordings({
                        rank: $scope.study.rank,
                        title: $scope.study.name,
                        description: $scope.studyDescription,
                        author: $scope.study.author,
                        user :  $scope.user._id,
                        username: $scope.user.username,
                        data: dataURL,
                        testChecklist : $scope.study.testChecklist
                    });

                    recording.$save(function(response){
                        $location.path('/');
                    }, function (errorResponse) {
                        console.log(errorResponse);
                    });
                    
                });
            }, function (error) {
                $location.path('/');
                console.log('close without save');
            });
        }

        $rootScope.$on('ngDialog.opened', function (e, $dialog) {
            $('#study-dialog audio').remove();
            
            var audio = document.createElement("audio");
            audio.setAttribute("controls", "controls");
            var src = document.createElement("source");
            src.setAttribute('src', $rootScope.audio);
            src.setAttribute('type', 'audio/ogg');
            audio.appendChild(src);
            var elem = document.getElementById('study-dialog');
            elem.appendChild(audio);
            
            audio.addEventListener('loadedmetadata', function() {
                
                var duration = audio.duration;
                var perfectDuration = $scope.study.perfectDuration;
                console.log(duration);
                console.log(perfectDuration);
                //aantal seconden ernaast (niet letterlijk want aangepast)
                var missedSeconds = Math.abs(perfectDuration - duration);
                
                $scope.percentageDuration = Math.round(100 - missedSeconds * 4);
                $scope.totalPointsTempo = $scope.study.checklist.tempo;
                $scope.pointsTempo = Math.round($scope.percentageDuration / 100 * $scope.totalPointsTempo);
                
                $scope.earnedPercentage = ($scope.pointsNotes + $scope.pointsTempo) / 40 * 100;
                $scope.earnedPoints = Math.round($scope.study.maxPoints * ($scope.earnedPercentage/100));
                
                $scope.$apply();
                
                $scope.user = Gamification.checkForLevelUp($scope.user, $scope.earnedPoints)
                .then(function(updatedUser){
                    console.log(updatedUser);
                    $scope.user = updatedUser;
                    updateUserAndGoBack(updatedUser);
                });           
            });
        });
        
        function updateUserAndGoBack(updatedUser){
            Users.get({
                userId: $rootScope.user._id
            }).$promise.then(function(user){
                user.profile = updatedUser.profile;
                user.level = updatedUser.level;
                
                user.$update(function() {
                    }, function(errornote) {
                        $scope.error = errornote.data.message;
                    }
                )}
                
            );
       }
    
        $scope.setActive = function(event){
            $('.instrument li.active').removeClass("active");
            $(event.target).addClass('active');
        }
    }
})();
