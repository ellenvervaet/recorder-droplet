;(function() {
    'use strict';
    angular.module('app.studies')
    .config(Routes);
    
    function Routes($routeProvider) {
        
        $routeProvider.
        when('/pick-study', {
            templateUrl: 'templates/studies/views/pick-study.client.view.html',
            controller: 'PickAStudyController'
        }).
        when('/study/:studyId', {
            templateUrl: 'templates/studies/views/study.client.view.html',
            controller: 'StudyController'
        });
        
    }
})();

