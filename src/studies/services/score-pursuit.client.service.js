;(function() {
    'use strict';
    angular.module('app.studies')
    .factory('ScorePursuit', ScorePursuit);
    
    function ScorePursuit($q, ngDialog){
        var audioContext = new AudioContext(),
            MAX_SIZE = Math.max(4,Math.floor(audioContext.sampleRate/5000)), // corresponds to a 5kHz signal
            isPlaying = false,
            analyser = null, 
            mediaStreamSource, //Moet hier, want anders komt het te vroeg in de garbage collector waardoor het stopt met luisteren
            pitch,
            noteToLearn,
            publicNote = "Do", //de noot die gespeeld wordt
            myVar, //timer
            ac, //is er een noot?
            octaaf, //octaaf van de gespeelde noot (alt != sopraan)
            buf = new Float32Array( 1024 ),
            countDown = 5,
            publicScore,
            publicKey,
            publicTime,
            finished = false,
            dialogClosed = true,
            noteBefore = "x";    
            
        var service = {
            noteToLearn: noteToLearnGS,
            myVar: myVarGS,
            dialogIsClosed : dialogIsClosed,
          
            drawScore: drawScore,   
            gotStream: gotStream,
            updatePitch: updatePitch,
            meetDoel: meetDoel
        };
        
        return service;
                
        //=============================
        //DECLARE VARIABLES
        //=============================
        window.AudioContext = window.AudioContext || window.webkitAudioContext;

        
        
        function noteToLearnGS(note){
            noteToLearn = note;
        }
        function myVarGS(vari){
            myVar = vari;
        }
        function dialogIsClosed(){
            dialogClosed = true;
            finished = false;
        }
        
        //=============================
        //PUBLIC FUNCTIONS
        //=============================
            
        function drawScore(notes, key, time) {
            publicScore = notes;
            publicKey = key;
            publicTime = time;
            var vextab = VexTabDiv;
            var VexTab = vextab.VexTab;
            var Artist = vextab.Artist;
            var Renderer = Vex.Flow.Renderer;
            
            // Create VexFlow Renderer from canvas element with id #boo
            var renderer = new Renderer($('#score')[0], Renderer.Backends.CANVAS);
        
            var width = document.getElementsByClassName('study-container')[0].clientWidth - 40;
            // Initialize VexTab artist and parser.
            var artist = new Artist(10, 10, width, {scale: 1});
            vextab = new VexTab(artist);

            try {
                
                //je hebt notes met alle noten in
                //knip hieruit de eerste 25 (indien er 25 zijn)
                //plak achteraan de uiteindelijke string    
                var twentyFiveNotes;
                var finalString = "";
                var countNotes = (notes.match(/\//g) || []).length;
                var lineIndex = 1;
                while(countNotes > 25){
                    var position = nth_occurrence(notes, '/', 25);
                    var index = 0;
                    while(notes.length > index){
                        if(notes.slice(position,position + 1) !== '|')
                        {
                            position--;
                        }
                        else{
                            position++;
                            break;
                        }
                        index++;
                    }
                    twentyFiveNotes = notes.substring(0, position);
                    notes = notes.replace(twentyFiveNotes, "");
                    finalString = finalString.concat("\n tabstave notation=true tablature=false time=" + time + " key=" + key + "\n notes " + twentyFiveNotes.trim() + " ")
                    countNotes = (notes.match(/\//g) || []).length;
                    if(lineIndex >= 3 && twentyFiveNotes.indexOf('$.top.$ $●$') >= 0){
                        // var div = document.getElementsByClassName('score')[0];
                        // div.scrollTop = 100 * (lineIndex - 2);
                        var scroll = 100 * (lineIndex - 2);
                        console.log(scroll);
                        $('div.score').stop(true,true).animate({ scrollTop: scroll}, 5000, "swing");
                        console.log('we zitten op lijn ' + lineIndex);
                    }
                    
                    lineIndex++;
                }
                finalString = finalString.concat("\n tabstave notation=true tablature=false time=" + time + " key=" + key + "\n notes " + notes.trim());
                
                //indien dit boven een bepaald aantal is
                // neem de nde figuur en zoek naar de | ervoor
                // voeg hierna toe: "\n tabstave ....\n notes"
                // dit doe je opnieuw voor de notes op de laatste rij
                
                // Parse VexTab music notation passed in as a string.
                vextab.parse(finalString + "=|=");

                // Render notation onto canvas.
                artist.render(renderer);
            } catch (e) {
                console.log(e);
            }
            
        }

        function gotStream(stream) {
            // Create an AudioNode from the stream.
            mediaStreamSource = audioContext.createMediaStreamSource(stream);

            //The createAnalyser() method of the AudioContext interface creates an AnalyserNode, 
            //which can be used to expose audio time and frequency data and create data visualisations.
            analyser = audioContext.createAnalyser();
            analyser.fftSize = 2048;
            //Connect the two
            mediaStreamSource.connect( analyser );   
        }

        function updatePitch() {
            return $q( function(resolve, reject) {
                analyser.getFloatTimeDomainData( buf );
                ac = autoCorrelate( buf, audioContext.sampleRate ); //autoCorrelate filtert ontoevalligheden

                //er is geen noot
                if (ac == -1) {
                    if(finished == true && dialogClosed == true){
                        //we willen van hier een functie uit de controller triggeren
                        $("#stopRecording").trigger("click");
                        
                        dialogClosed = false;
                    }
                    reject('geen geluid');
                }//er is een noot
                else {
                    //frequentie
                    pitch = ac;
                    
                    //NOOTNAAM IN HTML
                    //note is een afgerond getal. deel door 12 om de correcte index van notestrings te bekomen (er zijn nml 12 noten)
                    var noteStrings = ["C", "C#", "D", "E@", "E", "F", "F#", "G", "G#", "A", "B@", "B"];
                    var note =  noteFromPitch( pitch ); 

                    setOctave(note); //bereken de var octaaf
                    resolve(noteStrings[note%12] + '/' + octaaf);
                    
                    pursuit(publicScore, noteStrings[note%12] + '/' + octaaf);
                }
                    //zorgt ervoor dat updatePitch telkens opnieuw uitgevoerd wordt en het canvas opnieuw getekend
                    if (!window.requestAnimationFrame)
                        window.requestAnimationFrame = window.webkitRequestAnimationFrame;
                    var rafID = window.requestAnimationFrame( updatePitch );
                
            })
            
        }
        
        function pursuit(score, playedNote){
            var noteToPlay;
            var position;
            var noteAfter;
            //SET POSITION AND NOTE TO PLAY=============
            if(score.indexOf('$') == -1){//eerste noot is nog niet gespeeld
                noteToPlay = score.substring(0, 3); 
                position = 0;                
            }else{//eerste noot is gespeeld

                //waar we zitten in de partituur wordt aangegeven door deze string,
                //dit is het bolletje dat je ziet meebewegen (13 = lengte van deze string)
                position = score.indexOf("$.top.$ $●$") + 12;

                //de noot die we moeten spelen is dus na de positie te vinden
                noteToPlay = score.substring(position, position + 3);
            }
            
            var data = checkIfIsNote(noteToPlay, position, score);
            noteToPlay = data[0];
            position = data[1];
            
            var positionNoteAfter = position + 4;
            noteAfter = score.substring(positionNoteAfter, positionNoteAfter + 3);
            var dataNoteAfter = checkIfIsNote(noteAfter, positionNoteAfter, score);
            noteAfter = dataNoteAfter[0];
            positionNoteAfter = dataNoteAfter[1];
            
            var positionThirdNote = positionNoteAfter + 4;
            var thirdNote = score.substring(positionThirdNote, positionThirdNote + 3);
            var dataThirdNote = checkIfIsNote(thirdNote, positionThirdNote, score);
            thirdNote = dataNoteAfter[0];
            
            //CHECK IF THE PLAYED NOTE MATCHES THE NOTE TO PLAY AND FOLLOW
            //very first note, has to be correct
            if(noteToPlay == playedNote && score.indexOf('$') <= -1){
                $("#startRecording").trigger("click");
                score = score.slice(0, position + 3) + " $.top.$ $●$" + score.slice(position + 3);
                noteBefore = noteToPlay;
            }
            //playing the correct note
            else if(noteToPlay == playedNote && score.indexOf('$') > -1){
                score = score.replace(" $.top.$ $●$", "");
                score = score.slice(0, position - 9) + " $.top.$ $●$" + score.slice(position - 9);
                noteBefore = noteToPlay;
            }
            //played a wrong note
            else if(noteAfter == playedNote && score.indexOf('$') > -1 && thirdNote != noteBefore ){
                score = score.replace(" $.top.$ $●$", "");
                score = score.slice(0, positionNoteAfter - 9) + " $.top.$ $●$" + score.slice(positionNoteAfter - 9);
                noteBefore = noteAfter;
                var count = parseInt(document.getElementById('fouteNoten').innerHTML);
                count++;
                document.getElementById('fouteNoten').innerHTML = count;
            }
            if(position > score.length - 2){
                finished = true;
            }
                            
            drawScore(score, publicKey, publicTime);

        }
        
        function insertAtIndex(idx, rem, str) {
            return this.slice(0, idx) + str + this.slice(idx + Math.abs(rem));
        };

        function meetDoel(){
            return $q( function(resolve, reject) {
                var html = $("#countDown");
                html.html(countDown);
                //De juiste noot wordt gespeeld
                if(publicNote.toLowerCase() == noteToLearn.name && ac !== -1 && octaaf == noteToLearn.octave){
                    html.css("display", "block");
                    if(countDown == 0){
                        window.alert("Je kan een " + noteToLearn.name + " spelen, proficiat!");
                        window.clearInterval(myVar);
                        resolve('noot geleerd');
                    }else{
                        html.innerHTML = countDown;
                        countDown = countDown - 1;
                        reject('noot niet lang genoeg aangehouden');
                    }
                // De foute noot wordt gespeeld
                }else{
                    countDown = 5;
                    html.css("display", "none");
                    reject('de foute noot gespeeld');
                }
                
            });
        };
        
        //=============================
        //LOCAL FUNCTIONS
        //=============================

        function noteFromPitch( frequency ) {
            var noteNum = 12 * (Math.log( frequency / 440 )/Math.log(2) );
            return Math.round( noteNum ) + 69;
        }

        //100 cent is een halve noot
        //https://nl.wikipedia.org/wiki/Cent_%28muziek%29
        //formule die hier gebruikt wordt is een algemene formule die hierboven te vinden is
        function centsOffFromPitch( frequency, note ) {
            return Math.floor( 1200 * Math.log( frequency / frequencyFromNoteNumber( note ))/Math.log(2) );
        }

        function frequencyFromNoteNumber( note ) {
            return 440 * Math.pow(2,(note-69)/12);
        }

        function setOctave(note) {

            switch(true){
                    case (note>=60 && note<72):
                        octaaf = 3;
                        break;
                    case (note>=72 && note<84):
                        octaaf = 4;
                        break;
                    case (note>=84 && note<96):
                        octaaf = 5;
                        break;
                    case (note>=96 && note<108):
                        octaaf = 6;
                        break;
                }

                setOctaveToInstrument();

        }

        function drawPlayedNote(){
            
                var exerciseCanvas = $("#exerciseNote")[0];
                var exerciseVoice = new Vex.Flow.Voice({
                    num_beats: 1,
                    beat_value: 4,
                    resolution: Vex.Flow.RESOLUTION
                });
                var exerciseRenderer = new Vex.Flow.Renderer(exerciseCanvas, Vex.Flow.Renderer.Backends.CANVAS);
                var exerciseContext = exerciseRenderer.getContext();
                exerciseContext.clearRect(0, 0, exerciseCanvas.width, exerciseCanvas.height);
                var exerciseStave = new Vex.Flow.Stave(10, 0, 500);
                exerciseStave.addClef("treble").setContext(exerciseContext).draw();
                var noteArray = null;

                noteArray = getVexFlowNote();

                exerciseVoice.addTickables(noteArray);
                var formatter = new Vex.Flow.Formatter().joinVoices([exerciseVoice]).format([exerciseVoice], 250);
                exerciseVoice.draw(exerciseContext, exerciseStave);
                
        }

        function getVexFlowNote(){
            var noteArray;
            
            switch(publicNote){
                    case "Do":
                        noteArray = [ new Vex.Flow.StaveNote({ keys: ["c/" + octaaf], duration: "q"}) ];
                        break;
                    case "Do#":
                        noteArray = [ new Vex.Flow.StaveNote({ keys: ["c#/" + octaaf], 
                        duration: "q"}).addAccidental(0, new Vex.Flow.Accidental("#")) ];
                        break;
                    case "Re":
                        noteArray = [ new Vex.Flow.StaveNote({ keys: ["d/" + octaaf], duration: "q"}) ];
                        break;
                    case "Mib":
                        noteArray = [ new Vex.Flow.StaveNote({ keys: ["eb/" + octaaf], duration: "q"}).addAccidental(0, new Vex.Flow.Accidental("b")) ];
                        break;
                    case "Mi":
                        noteArray = [ new Vex.Flow.StaveNote({ keys: ["e/" + octaaf], duration: "q"}) ];
                        break;
                    case "Fa":
                        noteArray = [ new Vex.Flow.StaveNote({ keys: ["f/" + octaaf], duration: "q"}) ];
                        break;
                    case "Fa#":
                        noteArray = [ new Vex.Flow.StaveNote({ keys: ["f#/" + octaaf], duration: "q"}).addAccidental(0, new Vex.Flow.Accidental("#")) ];
                        break;
                    case "Sol":
                        noteArray = [ new Vex.Flow.StaveNote({ keys: ["g/" + octaaf], duration: "q"}) ];
                        break;
                    case "Sol#":
                        noteArray = [ new Vex.Flow.StaveNote({ keys: ["g#/" + octaaf], duration: "q"}).addAccidental(0, new Vex.Flow.Accidental("#")) ];
                        break;
                    case "La":
                        noteArray = [ new Vex.Flow.StaveNote({ keys: ["a/" + octaaf], duration: "q"}) ];
                        break;
                    case "Sib":
                        noteArray = [ new Vex.Flow.StaveNote({ keys: ["bb/" + octaaf], duration: "q"}).addAccidental(0, new Vex.Flow.Accidental("b")) ];
                        break;
                    case "Si":
                        noteArray = [ new Vex.Flow.StaveNote({ keys: ["b/" + octaaf], duration: "q"}) ];
                        break;
                    default :
                        noteArray = [ new Vex.Flow.StaveNote({ keys: ["g/" + octaaf], duration: "q"}) ];
                        break;
                }
                
            return noteArray;
        }
        //pursuit functions to check if the noteToPlay is correct
        function checkIfIsNote(noteToPlay, position, score) {
                var isNotNote = true;
                //als we op een tijdsnotering of dergelijke komen, willen we deze overslaan.
                //dit kunnen er meerderer zijn, vandaar de while
                if(noteToPlay.substring(noteToPlay.length -2, noteToPlay.length - 1) == "/" || noteToPlay == ""){
                    isNotNote = false;
                }
                while(isNotNote == true){
                //check if the note is an note and adapt if neccissary 
                var data = isNoteBar(noteToPlay, position, score);
                data = isNoteTiming(data[0], data[1], score);
                data = hasNoteSymbol(data[0], data[1], score);
                noteToPlay = data[0];
                position = data[1];
                //is the note a note now or do we have to do the while again?
                    if(noteToPlay.substring(noteToPlay.length - 2, noteToPlay.length - 1) == "/"){
                        isNotNote = false;
                    }
                }
                return [noteToPlay, position];
            }
        function isNoteBar(noteToPlay, position, score){
                    if(noteToPlay.indexOf('|') > -1){//de noteToPlay mag geen maatstreep zijn
                        position = position + 2;
                        noteToPlay = score.substring(position, position + 3);
                    }
                    return [noteToPlay, position];
                }
                function isNoteTiming(noteToPlay, position, score){
                    if(noteToPlay.indexOf(':8') > -1 || //mag geen achtste zijn
                        noteToPlay.indexOf(':q') > -1 || //mag geen kwart zijn
                        noteToPlay.indexOf(':h') > -1){ //mag geen hele zijn

                            if( noteToPlay.substring(2, 3) == "d"){  //gepunteerden zijn een karakter langer   
                                position = position + 4;
                            }
                            
                            else if(score.substring(position + 1, position +2) == "8" ||
                                    score.substring(position + 1, position +2) == "q" ||
                                    score.substring(position + 1, position +2) == "h"){
                                position = position + 3;
                            }
                            
                            noteToPlay = score.substring(position, position + 3);
                    }
                    if(noteToPlay.indexOf(':1') > -1){
                        position = position + 4;
                        noteToPlay = score.substring(position, position + 3);
                    }
                    return [noteToPlay, position];
                }
                function hasNoteSymbol(noteToPlay, position, score){
                    if(score.substring(position + 1, position +2) == "#" ||
                        score.substring(position + 1, position +2) == "n" ||
                        score.substring(position + 1, position +2) == "@"){
                            noteToPlay = score.substring(position, position + 4);
                            position++;
                    }
                    return [noteToPlay, position];
                }

        //toevalligheden
        function autoCorrelate( buf, sampleRate ) {
            var SIZE = buf.length;
            var MAX_SAMPLES = Math.floor(SIZE/2);
            var best_offset = -1;
            var best_correlation = 0;
            var rms = 0;
            var foundGoodCorrelation = false;
            var correlations = new Array(MAX_SAMPLES);
            var MIN_SAMPLES = 0;  // will be initialized when AudioContext is created.

            for (var i=0;i<SIZE;i++) {
                var val = buf[i];
                rms += val*val;
            }
            rms = Math.sqrt(rms/SIZE);
            if (rms<0.01) // not enough signal
                return -1;

            var lastCorrelation=1;
            for (var offset = MIN_SAMPLES; offset < MAX_SAMPLES; offset++) {
                var correlation = 0;

                for (var i=0; i<MAX_SAMPLES; i++) {
                    correlation += Math.abs((buf[i])-(buf[i+offset]));
                }
                correlation = 1 - (correlation/MAX_SAMPLES);
                correlations[offset] = correlation; // store it, for the tweaking we need to do below.
                if ((correlation>0.9) && (correlation > lastCorrelation)) {
                    foundGoodCorrelation = true;
                    if (correlation > best_correlation) {
                        best_correlation = correlation;
                        best_offset = offset;
                    }
                } else if (foundGoodCorrelation) {
                    // short-circuit - we found a good correlation, then a bad one, so we'd just be seeing copies from here.
                    // Now we need to tweak the offset - by interpolating between the values to the left and right of the
                    // best offset, and shifting it a bit.  This is complex, and HACKY in this code (happy to take PRs!) -
                    // we need to do a curve fit on correlations[] around best_offset in order to better determine precise
                    // (anti-aliased) offset.

                    // we know best_offset >=1,
                    // since foundGoodCorrelation cannot go to true until the second pass (offset=1), and
                    // we can't drop into this clause until the following pass (else if).
                    var shift = (correlations[best_offset+1] - correlations[best_offset-1])/correlations[best_offset];
                    return sampleRate/(best_offset+(8*shift));
                }
                lastCorrelation = correlation;
            }
            if (best_correlation > 0.01) {
                // console.log("f = " + sampleRate/best_offset + "Hz (rms: " + rms + " confidence: " + best_correlation + ")")
                return sampleRate/best_offset;
            }
            return -1;
        //	var best_frequency = sampleRate/best_offset;
        }
        
        

        function nth_occurrence (string, char, nth) {
            var first_index = string.indexOf(char);
            var length_up_to_first_index = first_index + 1;

            if (nth == 1) {
                return first_index;
            } else {
                var string_after_first_occurrence = string.slice(length_up_to_first_index);
                var next_occurrence = nth_occurrence(string_after_first_occurrence, char, nth - 1);

                if (next_occurrence === -1) {
                    return -1;
                } else {
                    return length_up_to_first_index + next_occurrence;  
                }
            }
        }
        
        function setOctaveToInstrument(){
            var instrument = $('.instrument li.active').text();
            console.log($('.instrument li.active').text());
            console.log('octaaf eerst : ' + octaaf);
            switch(instrument){
                case "Sopranino":
                    break;
                case "Sopraan":
                    break;
                case "Alt":
                    octaaf = octaaf + 1
                    break;
                case "Tenor":
                    octaaf = octaaf + 1
                    break;
            }
            console.log(octaaf)
        }
        
        }
    
})();